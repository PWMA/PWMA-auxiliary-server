<?php
	header("Content-Type: application/json");
	require_once("./conf.php");
	$sql = open_db();

	if (isset($_REQUEST['create_screen'])) {
		$query = "CREATE TABLE screen(
		 id serial PRIMARY KEY,
		 username VARCHAR (50),
		 ip INET,
		 title VARCHAR (200),
		 created_on TIMESTAMP default statement_timestamp(),
		 last_update TIMESTAMP,
		 content JSON
		);";
		$res = $sql->sql_query($query);
		if (!$res) {file_put_contents("./screens/{$_REQUEST['name']}.json", "$query<br>\nErrormessage: %s\n".$sql->sql_error());}
	}
	if (isset($_REQUEST['save'])) {
		session_start();
		if (!isset($_SESSION['token'])) $username = 'emiliano.coghetto'; // die(json_encode('Error: user not logged.'));
		else {
			$webtokenExpirationSeconds = 28800; // 8 hh
			$stmt = $sql->sql_prepare("SELECT * FROM userlog WHERE webtoken=$1 AND EXTRACT(EPOCH FROM current_timestamp-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", "q1");
			$data = $sql->sql_execute(array($_SESSION['token']), "q1");
			if (!empty($data)) die(json_encode('Error: invalid user login.'));
			$username = $data[0]['username'];
			if (strpos($_REQUEST['name'], '.')!==false) die(json_encode('Invalid screen name.'));
		}
		$stmt = $sql->sql_prepare("INSERT INTO screen(username,ip,title,content) VALUES($1, $2, $3, $4)", "q2");
		$params = array($username, $_SERVER['REMOTE_ADDR'], $_REQUEST['name'], $_REQUEST['content']);
		$data = $sql->sql_execute($params, "q2");

		/*
		$query = "INSERT INTO screen(username,ip,title,content) VALUES(NULL, '{$_SERVER['REMOTE_ADDR']}', '{$_REQUEST['name']}', '".json_encode($_REQUEST['content'])."');";
		$res = $sql->sql_query($query);
		if (!$res) {file_put_contents("./screens/{$_REQUEST['name']}.json", "$query<br>\nErrormessage: %s\n".$sql->sql_error());exit();}
		*/
		// file_put_contents("./screens/{$_REQUEST['name']}.json", $_REQUEST['content']);
		$err = $sql->sql_error();
		echo json_encode(array('name'=>$_REQUEST['name'], 'id'=>(int)$sql->last_insert_id(), 'err'=>$err));
	}

    if(isset($_REQUEST['update'])) {
		session_start();
		if (!isset($_SESSION['token'])) $username = 'emiliano.coghetto'; // die(json_encode('Error: user not logged.'));
		else {
			$webtokenExpirationSeconds = 28800; // 8 hh
			$stmt = $sql->sql_prepare("SELECT * FROM userlog WHERE webtoken=$1 AND EXTRACT(EPOCH FROM current_timestamp-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", "q1");
			$data = $sql->sql_execute(array($_SESSION['token']), "q1");
			if (!empty($data)) die(json_encode('Error: invalid user login.'));
			$username = $data[0]['username'];
			if (strpos($_REQUEST['name'], '.')!==false) die(json_encode('Invalid screen name.'));
		}
		$stmt = $sql->sql_prepare("UPDATE screen SET title = $2, content = $3 WHERE id = $1", "q2");
		$params = array($_REQUEST['id'], $_REQUEST['name'], $_REQUEST['content']);
		$data = $sql->sql_execute($params, "q2");
		echo json_encode(array('id'=>(int)$_REQUEST['id'],'name'=>$_REQUEST['name']));
    }

	if (isset($_REQUEST['list'])) {
		$orderby = 'title';
		$columns = array('id','title','username','created_on');
		if (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], $columns)) $orderby = $_REQUEST['orderby'];
		$desc = isset($_REQUEST['desc'])? " DESC": '';
		$s = array();
		$data = $sql->sql_select("id,title,username,created_on",'screen', "1=1 ORDER BY $orderby$desc");
		if (isset($_REQUEST['debug'])) echo "SELECT id,title,username,created_on FROM screen ORDER BY $orderby;<br>\n";
		foreach ($data as $d) {
			$s[] = array('id'=>(int)$d['id'],'title'=>empty($d['title'])? 'NO TITLE': $d['title'],'username'=>$d['username'],'created_on'=>$d['created_on']);
		}
		header("Content-Type: application/json");
		die(json_encode($s));
		// echo "<pre>\n"; print_r($data); echo "</pre>\n"; exit();
	}

	if (isset($_REQUEST['listScreens'])) {
		$dir = './screens';
		$list = array();
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				if (strpos($file, '.json')!==false) {
					$list[] = $file;
				}
			}
		}
		echo json_encode($list);
	}
	if (isset($_REQUEST['openScreenFile']) and (strpos($_REQUEST['openScreen'], '.json')!==false) and (strlen($_REQUEST['openScreen']) < 100)) readfile("./screens/".$_REQUEST['openScreen']);
	if (isset($_REQUEST['openScreen']) and (strlen($_REQUEST['openScreen']) < 100)) {
		$stmt = $sql->sql_prepare("SELECT * FROM screen WHERE title=$1", "q1");
		$params = array($_REQUEST['openScreen']);
		$data = $sql->sql_execute($params, "q1");
		die(json_encode(array('id'=>intval($data[0]['id']),'name'=>$data[0]['title'],'cmpts'=>json_decode($data[0]['content']))));
	}
    //TODO merge with openScreen
	if (isset($_REQUEST['openScreenId']) and (strlen($_REQUEST['openScreenId']) < 100)) {
		$stmt = $sql->sql_prepare("SELECT * FROM screen WHERE id=$1", "q1");
		$params = array($_REQUEST['openScreenId']);
		$data = $sql->sql_execute($params, "q1");
		die(json_encode(array('id'=>intval($data[0]['id']),'name'=>$data[0]['title'],'cmpts'=>json_decode($data[0]['content']))));
	}

if (isset($_REQUEST['ElettraStatus'])) readfile("./screens/ElettraStatus.json");
if (isset($_REQUEST['elettraStatus'])) readfile("./screens/ElettraStatus.json");
// if (isset($_REQUEST['ElettraStatus'])) echo '[{"component":"PwmaChart","title":"SR Current (last 8 hours)","src":"http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=elettra&start=last%208%20hours&ts=00046&no_pretimer&no_posttimer"},{"component":"PwmaContainer","label":"Status","children":[{"component":"PwmaScalar","label":"Current","unit":"mA","src":"tango://tom.ecs.elettra.trieste.it:20000/sr/DIAGNOSTICS/DCCT_S4/Current"},{"component":"PwmaScalar","label":"Energy","unit":"GeV","src":"tango://tom.ecs.elettra.trieste.it:20000/sr/DIAGNOSTICS/DCCT_S4/Energy"},{"component":"PwmaScalar","label":"Lifetime","unit":"h","src":"tango://tom.ecs.elettra.trieste.it:20000/sr/DIAGNOSTICS/DCCT_S4/LifetimeHours"}]},{"component":"PwmaEncodedArray","codeMap":{"1":{"false":{"value":"OK","backgroundColor":"green","width":90},"true":{"value":"NOK","backgroundColor":"red"}},"4":{"false":{"value":"Fault","backgroundColor":"#b00","color":"white","width":90},"true":{"value":"OK","backgroundColor":"green"}},"6":{"false":{"value":"Moving","backgroundColor":"#48b","width":90},"true":{"value":"---","backgroundColor":"white"}}},"src":"tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/Linac_board_disabled_008"}]'
// if (isset($_REQUEST['ElettraStatus'])) echo '[{"component":"PwmaChart","title":"SR Current (last 8 hours)","src":"http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/hdb_plot_service.php?conf=elettra&start=last%208%20hours&ts=00046&no_pretimer&no_posttimer"},{"component":"PwmaContainer","label":"Status","children":[{"component":"PwmaScalar","label":"Current","unit":"mA","src":"tango://tom.ecs.elettra.trieste.it:20000/sr/DIAGNOSTICS/DCCT_S4/Current"},{"component":"PwmaScalar","label":"Energy","unit":"GeV","src":"tango://tom.ecs.elettra.trieste.it:20000/sr/DIAGNOSTICS/DCCT_S4/Energy"},{"component":"PwmaScalar","label":"Lifetime","unit":"h","src":"tango://tom.ecs.elettra.trieste.it:20000/sr/DIAGNOSTICS/DCCT_S4/LifetimeHours"}]},{"component":"PwmaEncodedArray","codeMap":{1: { false: { "value": "OK", "backgroundColor": "green", "width": 90 }, true: { "value": "NOK", "backgroundColor": "red" } }, 4: { false: { "value": "Fault", "backgroundColor": "#b00", "color": "white", "width": 90 }, true: { "value": "OK", "backgroundColor": "green" } }, 6: { false: { "value": "Moving", "backgroundColor": "#48b", "width": 90 }, true: { "value": "---", "backgroundColor": "white" } }, },"src":"tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/Linac_board_disabled_008"}]'
if (isset($_REQUEST['acsboardselettra'])) {
	$var = array();
	$var['acsboosterboard229'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_229', 'labels'=>"ERR_EB_B12_1: errore pulsante emergenza");
	$var['acsboosterboard100'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_100', 'labels'=>"ERR_key_RFs: errore chiave RF service;ERR_key_1a: errore chiave 1a abilitazione;ERR_key_2a: errore chiave 2a abilitazione");
	$var['acsboosterboard253'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_253', 'labels'=>"ERR_BST_B14_1_CLOSED_MG: errore beam stopper chiuso;ERR_BST_BTS1_1_CLOSED_MG: errore beam stopper aperto;ERR_BST_BTS1_2_CLOSED_MG: errore beam stopper chiuso;ERR_BST_B14_1_OPEN_MG: errore BST aperto contatto magnetico;ERR_BST_BTS1_1_OPEN_MG: errore BST aperto contatto magnetic;ERR_BST_BTS1_2_OPEN_MG: errore BST aperto contatto magnetic");
	$var['acsboosterboard142'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_142', 'labels'=>"ERR_BOOSTER_TOP_UP_INJ: abilita chiusura ID senza disabilitare bst 2.3;ERR_BST_BL_ENABLE: apertura BST BL con i BST TL chiusi o in top up;ERR_SCR_A5_EN: errore scraper A5 abilitato;ERR_SCR_A7_EN: errore scraper A7 abilitato");
	$var['acsboosterboard130'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_130', 'labels'=>"ERR_DR_A12_1_CLOSED: porta emergenza Galleria Servizi SR A12 chiusa;ERR_DR_R3_1_CLOSED: errore porta esterna tetto storage ring;ERR_DR_R3_2_CLOSED: errore porta esterna tetto booster;ERR_INJ_ABI_SR: errore iniezione abilitata da SR (scraper ID, VLV, flussi)");
	$var['acsboosterboard112'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_112', 'labels'=>"ERR_DR_A10_1_CLOSED: porta emergenza Galleria Servizi SR A10 chiusa;ERR_DR_A8_1_CLOSED: porta emergenza Galleria Servizi SR A8 chiusa;ERR_BEAM_ABI_SR: errore SR pronto a ricevere fascio da controllo accessi SR");
	$var['acsboosterboard235'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_235', 'labels'=>"284.0;284.1;284.2;284.5");
	$var['acsboosterboard223'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_223', 'labels'=>"ERR_BST_B14_1_CLOSED_SW: errore BST chiuso contatto switch;ERR_BST_B14_1_OPEN_SW: errore BST aperto contatto switch;ERR_BST_BTS1_1_CLOSED_SW: errore BST chiuso contatto switch;ERR_BST_BTS1_1_OPEN_SW: errore BST aperto contatto switch");
	$var['acsboosterboard259'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_259', 'labels'=>"ERR_EB_B15_1: errore pulsante emergenza;ERR_EB_BTS1_1: errore pulsante emergenza;ERR_DR_BSA3_1_CLOSED: errore porta emergenza BSA 3.1 chiusa");
	$var['acsboosterboard118'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_118', 'labels'=>"ERR_BOOSTER_SAFE: errore booster off o tre bst chiusi");
	$var['acsboosterboard339'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_339', 'labels'=>"ERR_EB_BTS1_2: errore pulsante emergenza;ERR_BST_BTS1_3_CLOSED_MG: errore BST chiuso contatto magnetico;ERR_BST_BTS1_3_OPEN_MG: errore BST aperto contatto magnetico");
	$var['acsboard080'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/sr/accessi/pss/disabled_board_80', 'labels'=>"ABI_RF_9_fail: abilitazione cavita' RF 9 failure;ABI_RF_8_fail:abilitazione cavita' RF 8 failure;OPEN_CMD_BST_fail: uscita apertura beam stopper failure;BEAM_ABI_SR_fail:abilitazione anello da accessi SR_ verso booster e verso interlock failure;SWITCH_MOVE_ABI_fail: uscita abilitazione movimento switch failure;BST_CLOSED_fail: uscita beam stopper chiuso verso booster failure");
	$var['acsboosterboard291'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_291', 'labels'=>"ERR_EB_B26_1_1: errore pulsante emergenza;ERR_DR_BSA4_1_CLOSED: errore porta emergenza BSA 4.1 chiusa");
	$var['acsboosterboard124'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_124', 'labels'=>"ERR_BST_SR_CLD: errore beam stopper anello chiuso;ERR_RDA_TL_OK: errore ronda TL ok");
	$var['acsboosterboard064'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_064', 'labels'=>"ERR_GUN_OFF: errore gun iniezione spento;ERR_PS_HV_OFF: errore high voltage spento;ERR_MOD1HV_OFF: errore HV mod 1 spenta (sicurezza completa);ERR_MOD1RF_OFF: errore radio frequenza modulatore 1 spenta;ERR_MOD2HV_OFF: errore HV mod 2 spenta (sicurezza completa);ERR_MOD2RF_OFF: errore radio frequenza modulatore 2 spenta;ERR_SW1_M1_onDUMMY: errore contatto sw1 mod 1;ERR_SW1_M2_onDUMMY: errore contatto sw1 mod 2;ERR_SW2_M2_onDUMMY: errore contatto sw2 mod 1;ERR_SW2_M1_onDUMMY: errore contatto sw2 mod 2;ERR_FBKRF_OFF_S1: errore RF sup alla soglia in sezione 1;ERR_FBKRF_OFF_S2: errore RF sup alla soglia in sezione 2");
	$var['acsboosterboard321'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_321', 'labels'=>"ERR_NO_PUL_EME: errore Pulsante emergenza verso interlock (I44.2);ERR_H_V_ABIxINTCK: errore Abilita alta tensione gun per il modulatore");
	$var['acsboosterboard316'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_316', 'labels'=>"ERR_BO_HV_ABI: errore abilitazione HV cavita' radiofrequenza booster;ERR_BO_RF_ABI: errore abilitazione RF cavita' radiofrequenza booster");
	$var['acsboard048'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/sr/accessi/pss/disabled_board_48', 'labels'=>"DR_A3_fail: switch porta A3 failure;EB_SR_fail: cumulativo pulsanti emergenza SR failure;EB_TL_fail: cumulativo pulsanti emergenza TL failure;DR_E5_fail: switch porta E5 failure;DR_E4_fail: switch porta E4 failure;DR_T1_fail: switch porta T1 failure;I2D_TL_fail: switch porta intercaspedine I2D TL failure;FULL_EXT_KEY_A3_fail: ingresso cumulativo testimoni esterni A3 failure;INT_KEY_A3_fail:ingresso cumulativo testimoni interni A3 failure");
	$var['acsboosterboard333'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_333', 'labels'=>"ERR_BST_BTS1_3_CLOSED_SW: errore BST chiuso contatto switch;ERR_BST_BTS1_3_OPEN_SW: errore BST aperto contatto switch");
	$var['acsboosterboard080'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_080', 'labels'=>"ERR_GUN_ABI: errore abilitazione gun;ERR_PSHV_ABI: errore abilitazione H.V. iniettore;ERR_MOD1_HV_ABI: errore abilitazione parziale modulatore 1;ERR_MOD1_RF_ABI: errore abilitazione totale modulatore 1;ERR_MOD2_HV_ABI: errore abilitazione parziale modulatore 2;ERR_MOD2_RF_ABI: errore abilitazione totale modulatore 2;ERR_SWITCH_MOVE_ABI: errore abilitazione movimento switch R.F.");
	$var['acsboosterboard304'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_304', 'labels'=>"ERR_EB_B1_1: errore pulsante emergenza;ERR_DR_BSA1_1_CLOSED: errore porta emergenza BSA 1.1 chiusa;ERR_DR_R_CLOSED: errore porta tetto chiusa");
	$var['acsboosterboard148'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_148', 'labels'=>"ERR_EB_B7_1: errore pulsante emergenza");
	$var['acsboard024'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/sr/accessi/pss/disabled_board_24', 'labels'=>"ABI_RF_2_fail: abilitazione cavita' RF 2 failure;ABI_RF_3_fail: abilitazione cavita' RF 3 failure");
	$var['acsboosterboard345'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_345', 'labels'=>"ERR_BST_BSR1_3_OPENCMD: errore comando apertura beam stopper");
	$var['acsboard104'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/sr/accessi/pss/disabled_board_104', 'labels'=>"key_RFs_fail: chiave RF service failure;key_1a_ABI_fail: chiave 1a abilitazione failure");
	$var['acsboosterboard265'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_265', 'labels'=>"ERR_BST_BSR1_1_OPENCMD: errore comando apertura beam stopper;ERR_BST_BSR1_2_OPENCMD: errore comando apertura beam stopper;ERR_BSTB14_1_OPENCMD: errore comando apertura beam stopper");
	$var['acsboard008'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/sr/accessi/pss/disabled_board_08', 'labels'=>"DR_A2_fail: switch porta A2 failure;RF_2_OFF_fail: feedback R.F. 2 off failure;RF_3_OFF_fail: feedback R.F. 3 off failure;FULL_EXT_KEY_A2_fail:ingresso cumulativo testimoni esterni A2 failure;INT_KEY_A2_fail: ingresso cumulativo testimoni interni A2 failure");
	$var['acsboosterboard310'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_310', 'labels'=>"ERR_RF_B_PWR_OFF: errore RF booster completamente spenta;ERR_RF_B_OFF: errore radio frequenza booster spenta");
	$var['acsboard064'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/sr/accessi/pss/disabled_board_64', 'labels'=>"BST_S12_3_CLD_SW_fail: switch meccanico chiusura BST_12.3 failure;BST_S12_3_OPN_SW_fail: switch meccanicoapertura BST_12.3 failure;SW1_1_fail: switch radiofrequenza 1_1 failure;SW1_2_fail: switch radiofrequenza 1_2 failure;RF_PK_fail: Pick_up radiofrequenza failure;BOOSTER_SAFE_fail: ingresso booster safe failure;RING_ABI_BL_fail: ingresso abilitazione da linee failure;RF_8_OFF_fail1: feedback R.F. 8 off failure;RF_9_OFF_fail1: feedback R.F. 9 off failure;BST_S12_3_CLD_MG_fail: switch magnetico chiusura BST_12.3 failure;BST_S12_3_OPN_MG_fail: switch magnetico apertura BST_12.3 failure");
	$var['acsboosterboard008'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_008', 'labels'=>"ERR_DR_BRG1_1_CLOSED: errore porta booster chiusa;ERR_DR_A4_1_CLOSED: errore porta emergenza area servizi storage ring A4 chiusa;ERR_DR_A5_1_CLOSED: errore porta emergenza area servizi storage ring A5 chiusa;ERR_DR_A6_1_CLOSED: errore porta emergenza area servizi storage ring A6 chiusa;ERR_DR_A6_2_CLOSED: errore porta emergenza area trasformatore;ERR_INT_KEY_B_0: errore nessun testimone interno presente;ERR_FULL_EXT_KEY_B: errore tutti i testimoni esterni presenti");
	$var['acsboosterboard278'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_278', 'labels'=>"ERR_EB_B19_1: errore pulsante emergenza");
	$var['acsboosterboard247'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_247', 'labels'=>"ERR_BST_BTS1_2_CLOSED_SW: errore BST chiuso contatto switch;ERR_BST_BTS1_2_OPEN_SW: errore BST aperto contatto switch");
	$var['acsboosterboard136'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_136', 'labels'=>"ERR_BST_BTS2_1_CLOSED_SW: beam stopper chiuso contatti switch;ERR_BST_BTS2_1_OPEN_SW: beam stopper aperto contatti switch;ERR_BST_BTS2_1_OK: errore beam stopper 2.3 funzionamento OK;ERR_BST_BTS2_1_CLOSED_MG: beam stopper chiuso contatti magnetici;ERR_INJ_ABI_BL: errore iniezione abilitata da beam line;ERR_BST_BTS2_1_OPEN_MG: beam stopper aperto contatti magnetici");
	$var['acsboosterboard048'] = array('component'=>'PwmaLedArray', 'src'=>'tango://tom.ecs.elettra.trieste.it:20000/booster/accessi/plc_accessi/booster_board_disabled_048', 'labels'=>"ERR_DR_P1_1_CLOSED: errore porta iniezione chiusa;ERR_EB_P1_1: errore pulsante emergenza;ERR_EB_P1_2: errore pulsante emergenza;ERR_DR_CS4_1_CLOSED: errore porta esterna destra chiostrina 4.1 chiusa;ERR_DR_CS4_2_CLOSED1: errore porta esterna sinistra chiostrina 4.2 chiusa;ERR_INT_KEY_INJ_0: errore nessun testimone interno presente;ERR_FULL_EXT_KEY_INJ: errore tutti i testimoni esterni presenti");
	echo json_encode(array($var[$_REQUEST['acsboardselettra']]));
}
if (isset($_REQUEST['acsboardsfermi'])) {
	$var = array();
	$var['acsboard325'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_325', 'labels'=>"ERR_MODK14_HV_ABI;ERR_MODK14_RF_ABI;ERR_MODK15_HV_ABI;ERR_MODK15_RF_ABI");
	$var['acsboard222'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_222', 'labels'=>"ERR_MODK3_HV_ABI;ERR_MODK3_RF_ABI;ERR_MODK4_HV_ABI;ERR_MODK4_RF_ABI");
	$var['acsboard292'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_292', 'labels'=>"ERR_MODK9_HV_ABI;ERR_MODK9_RF_ABI;ERR_MODK10_HV_ABI;ERR_MODK10_RF_ABI");
	$var['acsboard280'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_280', 'labels'=>"ERR_MODK9_HV_OFF;ERR_MODK9_RF_OFF;ERR_MODK10_HV_OFF;ERR_MODK10_RF_OFF");
	$var['acsboard196'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_196', 'labels'=>"ERR_BST_L1_CLD_SW;ERR_BST_L1_OPEN_SW;ERR_BST_L1_CLD_MG");
	$var['acsboard158'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_158', 'labels'=>"ERR_EB_LT12_13;ERR_EB_LT14_15;ERR_INT_I2_EXT_CLD");
	$var['acsuhboard204'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_204', 'labels'=>"ERR_BST1_FEL1_CLD_SW: errore beam stopper SO1 chiuso;ERR_BST1_FEL1_OPEN_SW: errore beam stopper SO1 aperto;ERR_BST2_FEL1_CLD_SW: errore beam stopper SO2 chiuso;ERR_BST2_FEL1_OPEN_SW: errore beam stopper SO2 aperto;ERR_BST1_FEL2_CLD_SW: errore beam stopper SO3 chiuso;ERR_BST1_FEL2_OPEN_SW: errore beam stopper SO3 aperto;ERR_BST2_FEL2_CLD_SW: errore beam stopper SO4 chiuso;ERR_BST2_FEL2_OPEN_SW: errore beam stopper SO4 aperto;ERR_BST_DIAG_CLD_SW: errore beam stopper SO5 chiuso;ERR_BST_DIAG_OPEN_SW: errore beam stopper SO5 aperto;ERR_EB_SO6: errore pulsante di emergenza;");
	$var['acsboard216'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_216', 'labels'=>"ERR_MODK3_HV_OFF1;ERR_MODK3_RF_OFF1;ERR_MODK4_HV_OFF1;ERR_MODK4_RF_OFF1");
	$var['acsboard178'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_178', 'labels'=>"ERR_DR_E3_CLD;ERR_DR_E3_INT_CLD;ERR_INT_I1_E3_CLD;ERR_INT_I2_E3_CLD");
	$var['acsuhboard184'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_184', 'labels'=>"ERR_EB_SO4: errore pulsante di emergenza;ERR_EB_SO5: errore pulsante di emergenza");
	$var['acsboard058'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_058', 'labels'=>"ERR_SW1_1;ERR_SW1_2;ERR_SW2_1;ERR_SW2_2;ERR_SW3_1;ERR_SW3_2;ERR_SW4_1;ERR_SW4_2;ERR_SW5_1;ERR_SW5_2;ERR_SW6_1;ERR_SW6_2");
	$var['acsuhboard270'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_270', 'labels'=>"ERR_EB_ESA1: errore pulsante di emergenza;ERR_DR_ESA1_CLD: errore porta ESA1 chiusa");
	$var['acsboard078'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_078', 'labels'=>"ERR_MODK0_HV_OFF;ERR_MODK0_RF_OFF;ERR_MODK1_HV_OFF;ERR_MODK1_RF_OFF;ERR_MODK2_HV_OFF;ERR_MODK2_RF_OFF");
	$var['acsboard252'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_252', 'labels'=>"ERR_MODK6_HV_OFF1;ERR_MODK6_RF_OFF1;ERR_MODK7_HV_OFF1;ERR_MODK7_RF_OFF1");
	$var['acsuhboard008'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_008', 'labels'=>"ERR_DR_SO_1_CLOSED: errore porta sala ondulatori chiusa;ERR_EB_SO1: errore pulsante emergenza;ERR_DR_LIN_SO_CLD: errore porta linac/sala ondulatori chiusa;ERR_I1_SO_CLD: errore intercapedine porta SO chiusa;ERR_sp12441: errore spare;ERR_NO_DUMP_TST: errore (0 = chiave dump test inserita, segnale da Linac);ERR_prima_ABI_SO: errore 1 = prima abi, 0 = shutdown;ERR_LINAC_OFF: errore Linac sicuramente spento;ERR_BST_L_CLD: errore BM ST Linac sicuramente chiuso;ERR_GUN_OFF: errore Mod utilizzato come fotoinj. off;ERR_INT_KEY_SO_0: errore nessun testimone interno presente;ERR_FULL_EXT_KEY_SO: errore tutti i testimoni esterni presenti");
	$var['acsboard202'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_202', 'labels'=>"ERR_BST_L1_OPEN_MG;ERR_ABI_BST_L_SO;ERR_ABI_RF_L_SO;ERR_ABI_GUN_L_SO;ERR_BST_BL_CLD");
	$var['acsuhboard248'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_248', 'labels'=>"ERR_BST1_FEL1_OPEN_CMD: errore comando apertura BST;ERR_BST2_FEL1_OPEN_CMD: errore comando apertura BST;ERR_BST1_FEL2_OPEN_CMD: errore comando apertura BST;ERR_BST2_FEL2_OPEN_CMD: errore comando apertura BST;ERR_BST_DIAG_OPEN_CMD: errore comando apertura BST");
	$var['acsboard152'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_152', 'labels'=>"ERR_DR_E2_CLD;ERR_DR_E2_INT_CLD;ERR_INT_E2_SX_CLD;ERR_INT_E2_DX_CLD");
	$var['acsboard232'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_232', 'labels'=>"ERR_BST_L_CLD_SO_R;ERR_GUN_OFF_SO_R");
	$var['acsuhboard178'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_178', 'labels'=>"ERR_DR_E7_2_CLD: errore porta emergenza E7 chiusa;ERR_DR_E7_1_CLD: errore porta emergenza E7 interna chiusa;ERR_I1_E7_CLD: errore intercapedine E7 sinistra chiusa");
	$var['acsuhboard152'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_152', 'labels'=>"ERR_DR_E5_2_CLD: errore porta emergenza E5 chiusa;ERR_DR_E5_1_CLD: errore porta emergenza E5 interna chiusa;ERR_I2_E5_CLD: errore intercapedine E5 sinistra chiusa;ERR_I1_E5_CLD: errore intercapedine E5 destra chiusa");
	$var['acsuhboard126'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_126', 'labels'=>"ERR_DR_E6_2_CLD: errore porta emergenza E6 chiusa;ERR_DR_E6_1_CLD: errore porta emergenza E6 interna chiusa;ERR_I1_E6_CLD: errore intercapedine E6 sinistra chiusa;ERR_I2_E6_CLD: errore intercapedine E6 destra chiusa");
	$var['acsboard258'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_258', 'labels'=>"ERR_MODK8_HV_OFF;ERR_MODK8_RF_OFF");
	$var['acsboard240'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_240', 'labels'=>"ERR_MODK5_HV_OFF1;ERR_MODK5_RF_OFF1");
	$var['acsuhboard158'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_158', 'labels'=>"ERR_EB_SO7: errore pulsante di emergenza;ERR_EB_SO8: errore pulsante di emergenza;ERR_EB_SO9: errore pulsante di emergenza;ERR_EB_SO10: errore pulsante di emergenza");
	$var['acsboard264'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_264', 'labels'=>"ERR_MODK6_HV_ABI;ERR_MODK6_RF_ABI;ERR_MODK7_HV_ABI;ERR_MODK7_RF_ABI");
	$var['acsboard227'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_227', 'labels'=>"ERR_BST_L1_OPENCMD1");
	$var['acsboard269'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_269', 'labels'=>"ERR_MODK8_HV_ABI;ERR_MODK8_RF_ABI");
	$var['acsboard106'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_106', 'labels'=>"ERR_DR_MAIN_CLD;ERR_DR_MAIN_BLK;ERR_EB_LT01_21_22;ERR_EB_LT02_03");
	$var['acsboard297'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_297', 'labels'=>"ERR_MODK11_HV_ABI;ERR_MODK11_RF_ABI");
	$var['acsboard184'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_184', 'labels'=>"ERR_EB_L16_171;ERR_EB_L18_bis1;ERR_EB_TL1");
	$var['acsuhboard288'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_288', 'labels'=>"ERR_EB_ESA2: errore pulsante di emergenza;ERR_DR_ESA2_CLD: errore porta ESA2 chiusa");
	$var['acsboard126'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_126', 'labels'=>"ERR_DR_E1_CLD;ERR_DR_E1_INT_CLD;ERR_INT_E1_SX_CLD;ERR_INT_E1_DX_CLD");
	$var['acsboard068'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_068', 'labels'=>"ERR_SW7_1;ERR_SW7_2;ERR_SW8_1;ERR_SW8_2;ERR_PKRF_OFF_CTF1;ERR_PKRF_OFF_CTF2;ERR_PKRF_OFF_PC_GUN;ERR_PKRF_OFF_ACCT_L00_01;ERR_PKRF_OFF_ACCT_L00_02;ERR_PKRF_OFF_LERDF");
	$var['acsboard320'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_320', 'labels'=>"ERR_MODK12_HV_ABI1;ERR_MODK12_RF_ABI1;ERR_MODK13_HV_ABI1;ERR_MODK13_RF_ABI1");
	$var['acsuhboard024'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_024', 'labels'=>"ERR_ABI_BST_L: errore abilitazione BST Linac verso accessi linac;ERR_ABI_RF_L: errore abilitazione RF verso acc. linac;ERR_ABI_GUN_L: errore abilitazione accensione GUN verso acc. linac;ERR_BST_BL_CLD: errore BST beam line tutti chiusi verso accessi linac");
	$var['acsboard314'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_314', 'labels'=>"ERR_MODK14_HV_OFF;ERR_MODK14_RF_OFF;ERR_MODK15_HV_OFF;ERR_MODK15_RF_OFF");
	$var['acsuhboard132'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_132', 'labels'=>"ERR_EB_SO2: errore pulsante di emergenz;ERR_EB_SO3: errore pulsante di emergenz");
	$var['acsboard132'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_132', 'labels'=>"ERR_EB_LT9;ERR_EB_LT10_11;ERR_INT_I1_EXT_CLD");
	$var['acsuhboard106'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_106', 'labels'=>"ERR_EB_SO11: errore pulsante di emergenza;ERR_EB_SO12: errore pulsante di emergenza;ERR_EB_SO13: errore pulsante di emergenza");
	$var['acsboard040'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_040', 'labels'=>"ERR_Prima_ABI;ERR_NO_DUMP_TEST;ERR_prima_ABI_SO");
	$var['acsboard088'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_088', 'labels'=>"RR_MODK0_HV_ABI1;RR_MODK0_RF_ABI1;RR_MODK1_HV_ABI1;RR_MODK1_RF_ABI1;RR_MODK2_HV_ABI1;RR_MODK2_RF_ABI1");
	$var['acsboard100'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_100', 'labels'=>"ERR_DR_E0_CLD;ERR_DR_E0_INT_CLD;ERR_INT_E0_SX_CLD;ERR_INT_E0_DX_CLD");
	$var['acsboard246'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_246', 'labels'=>"ERR_MODK5_HV_ABI;ERR_MODK5_RF_ABI");
	$var['acsboard008'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_008', 'labels'=>"ERR_DR_L1_1;ERR_EB_LT04;ERR_EB_LT05_06;ERR_EB_LT07_08;ERR_EB_LT19_20;ERR_I3_E0_CLD;ERR_INT_KEY_L_0;ERR_FULL_EXT_KEY_L");
	$var['acsuhboard214'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_214', 'labels'=>"ERR_BST1_FEL1_CLD_MG: errore beam stopper SO1 chiuso;ERR_BST2_FEL1_CLD_MG: errore beam stopper SO2 chiuso;ERR_BST1_FEL2_CLD_MG: errore beam stopper SO3 chiuso;ERR_BST2_FEL2_CLD_MG: errore beam stopper SO4 chiuso;ERR_BST_DIAG_CLD_MG: errore beam stopper SO5 chiuso;ERR_BST1_FEL1_OPEN_MG: errore beam stopper SO1 aperto;ERR_BST2_FEL1_OPEN_MG: errore beam stopper SO2 aperto;ERR_BST1_FEL2_OPEN_MG: errore beam stopper SO3 aperto;ERR_BST2_FEL2_OPEN_MG: errore beam stopper SO4 aperto;ERR_BST_DIAG_OPEN_MG: errore beam stopper SO5 aperto");
	$var['acsboard286'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_286', 'labels'=>"ERR_MODK11_HV_OFF;ERR_MODK11_RF_OFF");
	$var['acsuhboard100'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/undulator_board_disabled_100', 'labels'=>"ERR_DR_E4_2_CLD: errore porta emergenza E4 chiusa;ERR_DR_E4_1_CLD: errore porta emergenza E4 interna chiusa;ERR_I2_E4_CLD: errore intercapedine E4 sinistra chiusa;ERR_I1_E4_CLD: errore intercapedine E4 destra chiusa");
	$var['acsboard308'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_308', 'labels'=>"ERR_MODK12_HV_OFF;ERR_MODK12_RF_OFF;ERR_MODK13_HV_OFF;ERR_MODK13_RF_OFF");
	$var['acsboard190'] = array('component'=>'PwmaLedArray', 'src'=>'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/f/access_control/safety/linac_board_disabled_190', 'labels'=>"ERR_DR_T1_CLD;ERR_DR_SO1_CLD;ERR_DR_INT_T1_CLD;ERR_DR_INT_TL_1_CLD");
	echo json_encode(array($var[$_REQUEST['acsboardsfermi']]));
}
?>
