<?php
	session_start();
	$username = '';
	if (isset($_SESSION['token'])) {
		require_once("./conf.php");
		$sql = open_db();
		$yy = date('Y');
		$webtokenExpirationSeconds = 36000; // 10 hh
		$data = $sql->sql_secure("SELECT * FROM userlog_$yy WHERE webtoken=$1 AND EXTRACT(EPOCH FROM NOW()-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", array($_SESSION['token']));
		if (!empty($data)) $username = $data[0]['username'];
	}
	// state https://tango-controls.readthedocs.io/en/latest/development/device-api/ds-guideline/device-server-guidelines.html#states-choice
	// https://tango-controls.readthedocs.io/en/latest/development/client-api/cpp-client-programmers-guide.html#data-types
	$types = array(
		"DevVoid",
        "DevBoolean",
        "DevShort",
        "DevLong",
        "DevFloat",
        "DevDouble",
        "DevUShort",
        "DevULong",
        "DevString",
        "DevVarCharArray",
        "DevVarShortArray", //10
        "DevVarLongArray",
        "DevVarFloatArray",
        "DevVarDoubleArray",
        "DevVarUShortArray",
        "DevVarULongArray",
        "DevVarStringArray",
        "DevVarLongStringArray",
        "DevVarDoubleStringArray",
        "DevState",
        "ConstDevString", // 20
        "DevVarBooleanArray",
        "DevUChar",
        "DevLong64",
        "DevULong64",
        "DevVarLong64Array",
        "DevVarULong64Array",
        "DevInt",
        "DevEncoded",
        "DevEnum",
        "DevPipeBlob", // 30
        "DevVarStateArray",
        "Unknown"
	);
	$type_num = file_get_contents("https://pwma.elettra.eu:10443/v1/cs/{$_REQUEST['varDetail']}/in_type")-0;
	$type_coded = $types[$type_num];
	$type_desc = file_get_contents("https://pwma.elettra.eu:10443/v1/cs/{$_REQUEST['varDetail']}/in_type_desc");
	$is_number = in_array($type_num, array(2,3,4,5,6,7,23,24,27));
?>



<!-- Access-Control-Allow-Origin: *
Access-Control-Allow-Methods: GET, POST, PUT, DELETE
Access-Control-Allow-Headers: Authorization-->
<!-- jquery -->
<link rel="stylesheet" href="../formulaeditor/lib/jquery/jquery-ui.min.css">
<script src="../formulaeditor/lib/jquery/jquery.min.js" type="text/javascript"></script>
	<!-- Bootstrap core CSS -->
	<link href="../formulaeditor/lib/bootstrap/bootstrap.css" rel="stylesheet">
	<!-- Bootstrap theme -->
	<link href="../formulaeditor/lib/bootstrap/bootstrap-theme.css" rel="stylesheet">

	<!-- Bootstrap core JavaScript -->
	<script src="../formulaeditor/lib/bootstrap/bootstrap.js"></script>
	<script src="../formulaeditor/lib/bootstrap/docs.js"></script>
<div style='width:100%; padding:10px'>
<h4 id='cmdName'></h4>
<!-- TODO: add JQuery UI suggestion -->

	
<?php 
	if (empty($username))
		echo "Username <input type='text' class='form-control' id='username' suggested='name.surname' />\nPassword <input type='password' class='form-control' id='password' />\n";
	else 
		echo "All operations are logged reporting username: <span style='background-color: darkgreen; color: white; padding: 5px; font-weight: bold; border-radius: 10px;'>$username</span><br>";

	echo $type_coded=='DevVoid'? 'Argin type: DevVoid': "Argin value (<span style='background-color: #e0e0e0;'>$type_coded - $type_desc</span>) <input type='text' class='form-control' id='argin' />";
?>

	
<br>
<input type="submit" onClick="sendStart()" value='Execute' />
<div id='results'></div>
<script>
	var $_GET = getQueryParams(document.location.search);
	var url = $_GET['varDetail'];
	$('#cmdName').html($_GET['varDetail']);
	console.log('_GET',$_GET);

	function getFullMonth(d) {
		let m = d.getMonth() + 1;
		if (m<10) m = '0' + m;
		return m;
	}
	function getFullDate(d) {
		let dd = d.getDate();
		if (dd<10) dd = '0' + dd;
		return dd;
	}
	function userDate(d) {
		var date = d.getFullYear() + '-' + getFullMonth(d) + '-' + getFullDate(d);
		let time = d.toLocaleTimeString().split(' ')[0];
		if (time.length < 8) time = '0'+time;
		return date + ' ' + time;
	}

	function sendStart() {
		var loggedin = <?php echo empty($username)? 'false': 'true'; ?>;
		var headers = new Headers();
		console.log('argin', JSON.stringify({value: $('#argin').val()-0}));
		if (!loggedin) {
			headers.append("Authorization", "Basic " + btoa(unescape(encodeURIComponent($('#username').val().split('@')[0] + ':' + $('#password').val()))));
			var startTime = new Date();
			fetch(`https://pwma.elettra.eu/cmd.php?cmd=${url}&params=${$('#argin').val()}`, {
				  method: 'POST',
				  headers: headers,
				  // body: JSON.stringify({value: $('#argin').val()-0}),
			})
			.then(resp => {
				console.log('response', resp);
				if (resp.status < 300) return resp.json();
				return {acknowledged: 'unknown', last_occurrence: 'unknown'};
			})
			.then((respJson) => {
				console.log('respJson', respJson);
				var endTime = new Date();
				var timeDiff = endTime - startTime;
				if (respJson.error) {
					$('#results').prepend('ERROR: '+respJson.error+'<br>');
				}
				else {
					$('#results').prepend(respJson.time+' duration: '+timeDiff+' ms '+(respJson.result=='null'? 'Command OK<br>': '<pre>'+jQuery.parseJSON(respJson.result).data.value+'</pre>'));
				}
				console.log('respJson', jQuery.parseJSON(respJson.result), {t: timeDiff, response: respJson});
			})
			.catch(err => {
				console.log(`ERR: `,err);
			});
		}
		else {
			var startTime = new Date();
			var mytime = userDate(startTime);
			var argin = $('#argin')? $('#argin').val() <?php if ($is_number) echo "-0"; ?>: undefined;
			
			console.log('argin', $('#argin')? $('#argin').val()-0: undefined);
			/*
			fetch(`https://pwma.elettra.eu:10443/v1/cs/<?php echo $_REQUEST['varDetail'] ?>`, {
				  method: 'POST',
				  body: JSON.stringify({value: $('#argin')? $('#argin').val(): undefined})
			})
			*/
			fetch(`https://pwma.elettra.eu/cmd.php?cmd=${url}&params=${argin}`)
			/*fetch('https://pwma.elettra.eu:10443/v1/cs/<?php echo $_REQUEST['varDetail']; ?>', {
				method: 'POST',
				body: JSON.stringify({value: argin})
			})*/
			.then(resp => {
				console.log('response', resp);
				if (resp.status < 300) return resp.json();
				return resp.text();
			})
			.then((respJson) => {
				console.log('respJson1', respJson);
				var endTime = new Date();
				var timeDiff = endTime - startTime;
				var result = '';
				if (respJson.error) {
					$('#results').prepend('ERROR: '+respJson.error+'<br>');
				}
				else {
					$('#results').prepend(respJson.time+' duration: '+timeDiff+' ms '+(respJson.result=='null'? 'Command OK<br>': '<pre>'+jQuery.parseJSON(respJson.result).data.value+'</pre>'));
				}
				console.log('respJson', {t: timeDiff, response: respJson});
			})
			.catch(err => {
				console.log(`ERR: `,err);
			});
			
		}
	}

	function getQueryParams(qs) {
		qs = qs.split("+").join(" ");
		var params = {},
						tokens,
						re = /[?&]?([^=]+)=([^&]*)/g;
		while (tokens = re.exec(qs)) {
				params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
		}
		return params;
	};
</script>
