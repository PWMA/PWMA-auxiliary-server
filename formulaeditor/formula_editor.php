<!DOCTYPE html>
<html lang="en"><head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="LZ">
	<link rel="icon" href="http://www.elettra.eu/favicon.png">

	<title>Alarm Editor</title>
	<style type="text/css">
		ul.fancytree-container {
			width: 220px;
			height: 450px;
			overflow: auto;
			position: relative;
		}
	</style>
	<script type="text/javascript"  async src="./lib/MathJax/MathJax.js?config=TeX-MML-AM_HTMLorMML"></script>

	<!-- Bootstrap core CSS -->
	<link href="./lib/bootstrap/bootstrap.css" rel="stylesheet">
	<!-- Bootstrap theme -->
	<link href="./lib/bootstrap/bootstrap-theme.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="./lib/bootstrap/theme.css" rel="stylesheet">

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./lib/bootstrap/ie10-viewport-bug-workaround.js"></script>

	<!-- jquery -->
	<link rel="stylesheet" href="./lib/jquery/jquery-ui.min.css">
	<script src="./lib/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="./lib/jquery/jquery-ui.min.js" type="text/javascript"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="./lib/bootstrap/bootstrap.js"></script>
	<script src="./lib/bootstrap/docs.js"></script>

<?php
	require_once("../conf.php");
	$old_error_reporting = error_reporting(E_ALL);
	require_once("../lib/sql_interface.php");
	error_reporting($old_error_reporting);
	$sql = new SqlInterface("pg");
	// connect to database
	$db = $sql->sql_connect(HOST, USERNAME, PASSWORD, DB);
	if (isset($_REQUEST['logout'])) {
		session_start();
		unset($_SESSION['token']);
	}
	session_start();

	function multiexplode ($delimiters,$string) {
		$ready = str_replace($delimiters, $delimiters[0], $string);
		$launch = explode($delimiters[0], $ready);
		return  $launch;
	}
	
	$formula = $name = $note = $dateexpiration = '';
	if (isset($_REQUEST['myformula'])) {
		$stmt = $sql->sql_prepare("SELECT * FROM formula WHERE label=$1", "q4");
		$data = $sql->sql_execute(array($_REQUEST['myformula']), "q4");
		$name = $data[0]['label'];

		$formulaArray = explode('tango://', $data[0]['formula']);
		foreach ($formulaArray as $k=>$f) {
			if ($k==0) continue;
			$t = explode('/', $f);
			$e = multiexplode(array(' ','<','>','+','-','+','*','|','&'),$t[4]);
			$t[4] = strtr($t[4], array($e[0]=>$e[0].'"'));
			$formulaArray[$k] = implode('/', $t);
		}
		$formula = implode('"tango://',$formulaArray);	
		// echo "<pre>"; print_r($formulaArray); echo "</pre>";
		$note = $data[0]['note'];
		$dateexpiration = substr($data[0]['dateexpiration'], 0, 10);
	}
	

	// ----------------------------------------------------------------
	// check access credentials
	function personal_alarms() {
		global $sql;
		$stmt = $sql->sql_prepare("SELECT label FROM formula, userlog WHERE formula.username=userlog.username AND webtoken=$1", "q3");
		$data = $sql->sql_execute(array($_SESSION['token']), "q3");
		echo "<select id='myformulae' onChange=\"window.location='?myformula='+document.getElementById('myformulae').value;\">\n<option value=\" \"> </option>";
		foreach ($data as $d) {
			echo "<option value=\"{$d['label']}\">{$d['label']}</option>\n";
		}
		echo "</select>\n";
	}
	

	// ----------------------------------------------------------------
	// check access credentials
	function check_access() {
		global $sql;
		$remote = $_SERVER['REMOTE_ADDR'];
		$forwarded = isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: 0;
		if (isset($_SESSION['token'])) {
			$webtokenExpirationSeconds = 28800; // 8 hh
			$stmt = $sql->sql_prepare("SELECT * FROM userlog WHERE webtoken=$1 AND EXTRACT(EPOCH FROM current_timestamp-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", "q1");
			$data = $sql->sql_execute(array($_SESSION['token']), "q1");
			if (!empty($data)) return;
		}
		if (!function_exists('ldap_connect')) die("LDAP module not installed in PHP");
		$ds=ldap_connect("abook.elettra.eu");  // must be a valid LDAP server!
		if (!$ds) { 
			die("<h4>Unable to connect to LDAP server</h4>");
		}
		if (isset($_REQUEST['username']) and isset($_REQUEST['elettra_ldap_password'])) {
			$email = $_REQUEST['username'];
			$password = $_REQUEST['elettra_ldap_password'];
			$r=ldap_bind($ds, $email, $password);  
			if ($r!="successful") die("login failed<br><br>");
			for ($i=0,$token=""; $i<5; $i++) {
				$token .= sprintf("%02x", rand(0, 256));
			}
			$_SESSION['token'] = $token;
			$stmt = $sql->sql_prepare("INSERT INTO userlog (date,webtoken,username,ip) VALUES (NOW(),$1,$2,$3)", "q2");
			$data = $sql->sql_execute(array($token, $_REQUEST['username'], $remote), "q2");
		}	
		else {
			// Prepare list of users
			$jsSearch = "	<script type=\"text/javascript\">\n	var userList = [\n";
			$f = file('http://fcsproxy.elettra.trieste.it/docs/pss/users.csv');	
			foreach ($f as $l) {$jsSearch .= "'".trim($l)."',\n";}
			$jsSearch .= "''";
			$jsSearch .= "];\n$( function() {\n $( \".username\" ).autocomplete({source: userList, select: function( event, ui ) {console.log('', ui.item.value);}});})\n	</script>\n";
			echo $jsSearch;
			die("User Validation<br><br><form method='post' action='?'>username <input type='text' class='username' name='username' id='username' value='' size='34' placeholder='name.surname' onChange=\"changeUser()\" required><br><br>password <input type='password' name='elettra_ldap_password'> <input type='submit' value='Login'></form><br /></body></html>\n");
		}
	}
	check_access();
?>


<script type="text/javascript">
<!-- hide from non JavaScript browsers ->
	// INIT
	
	var formula_edit = true;

	function checkKey(t, e) {
		e = e || window.event;
		if (e.keyCode < '37' || e.keyCode > '40') {
			processMathJax()
		}
	}

	function checkAlarmName() {
		document.getElementById('name').value = document.getElementById('name').value.substring(0, 15);
	}

	function substituteTs() { 
		for (i=0; i<document.getElementById('searchResults').length; i++) {
			if (document.getElementById('searchResults').options[i].selected) {
				replaceTs(document.getElementById('search').value, document.getElementById('searchResults').options[i].value);
			}
		}
	}

	function appendTs() { 
		for (i=0; i<document.getElementById('searchResults').length; i++) {
			if (document.getElementById('searchResults').options[i].selected) {
				document.formulae.formula.value += '"'+document.getElementById('searchResults').options[i].value+'"';
			}
		}
		processMathJax();
	}

	function searchTs() {
		var search = document.getElementById('search_similar').checked? '&search_similar=': '&search=';
		var request = treeService+search+document.getElementById("search").value;
		$.get(request, function(data) {
			var found = '';
			for (var key in data) {
				found += '<option value="'+ data[key] + '">' + data[key] + '</option>';
			}
			document.getElementById("searchResults").innerHTML = found;
		});
		// http://jqueryui.com/sortable/#connect-lists
	}
	
	function replaceTs(from, to) {
		document.getElementById("formula").value = document.getElementById("formula").value.replace(from, to);
		processMathJax();
	}

	// var searchWindow = null;
	function serachSimilar(search) {
		document.getElementById('search').value = search;
		document.getElementById('search_similar').checked = true;
		searchTs();
	}

	function exportFormula() {
		if (document.getElementById("formula").value.length<1) {window.alert('Please fill alarm formula with a valid expression'); return false;}
		if (document.getElementById("name").value.length<3) {window.alert('Please fill alarm name with at least 3 chars'); return false;}
		if (document.getElementById("name").value.length>15) {window.alert('Please fill alarm name with no more than 15 chars'); return false;}
		if (document.getElementById("dateexpiration").value.length > 0 && (document.getElementById("dateexpiration").value.length<10 || document.getElementById("dateexpiration").value[4]!=='-' || document.getElementById("dateexpiration").value[7]!=='-')) {
			window.alert('Please fill expiration date using format: YYYY-MM-DD'); return false;
		}
		const note = (document.getElementById("note").value.length>1) ? '&note=' + document.getElementById("note").value : null;
		const url = "https://pwma-dev.elettra.eu/pwma_alarms.php";
		const param = {save: '', name: document.getElementById("name").value, formula: document.getElementById("formula").value, dateexpiration: document.getElementById("dateexpiration").value, note: document.getElementById("note").value}
		// 
		console.log('exportFormula()', url, param);
		$.post(url, param, function(data) {
			console.log(data);
			window.alert(data);
			fetch("https://pwma-dev.elettra.eu:10443/v1/formula/"+encodeURI(document.getElementById("name").value),function(d) {
				console.log(d,"https://pwma-dev.elettra.eu:10443/v1/formula/"+encodeURI(document.getElementById("name").value));
				window.alert(d);
			})
			.catch(err => {
				window.alert(err);
			});
		});
		return false;
	}

	function processMathJax() {
		var math = document.getElementById('mathml');
		document.getElementById("inputText").value = "`"+document.getElementById("formula").value+"`";
		var mathML = AMviewMathML();
		// document.getElementById("inputText").value = document.getElementById("inputText").value.replace("`",'').replace("`",'');
		var mathToken = mathML.split('mtext>');
		var ts = [];
		for (var i=0, k=0; i<mathToken.length; i++) {
			if(i % 2 == 1) {ts[k++] = strtr(mathToken[i], {"</":'', ' ':''});}
		}
		// var mathMLreplace = {'<mtext>': '<mtext mathcolor="red" href="javascript:alert(\'time series\')">', '<mstyle displaystyle="true" mathcolor="blue" fontfamily="serif">':'', '<mstyle>': ''};
		var mathMLreplace = {'<mstyle displaystyle="true" mathcolor="blue" fontfamily="serif">':'', '<mstyle>': ''};
		// console.log(treeService+'&keys='+ts.join(';'));
		if (mathToken.length>1) {
			$.get(treeService+'&keys='+ts.join(';'), function(data) {
				if (typeof(data[0]) !== 'undefined') {
					var color = 'red';
					var script;
					for (var i=0; i<data.length; i++) {
						// alert(mathML); alert('<mtext>'+data[i].title);
						color = data[i].id!==null? 'green': 'red';
						script = data[i].id!==null? "alert('time series id: "+data[i].id+"')": "serachSimilar('"+data[i].title+"')";
						mathMLreplace['<mtext>'+data[i].title] = '<mtext mathcolor="'+color+'" href="javascript:'+script+'">'+data[i].title;
					}
				}
				math.innerHTML = strtr(mathML, mathMLreplace);
				MathJax.Hub.Queue(["Typeset",MathJax.Hub,math]);
			});
		}
		else {
			math.innerHTML = strtr(mathML, mathMLreplace);
			MathJax.Hub.Queue(["Typeset",MathJax.Hub,math]);
		}
		// <mi><mglyph src="my-glyph.png" alt="my glyph"/></mi>
	}

	function f(append) {
		document.formulae.formula.value += append;
		processMathJax();
	}
	function append2formula(append) {
		document.formulae.formula.value += append;
		processMathJax();
	}

	function p() {
		for (var i=0; i<document.formulae.point.length; i++) {
			if (document.formulae.point.options[i].selected) {
				f('"'+document.formulae.point.options[i].value+'"');
			}
		}
	}

	function handleFormulaSelect(evt) {
		evt.stopPropagation();
		evt.preventDefault();
		var ts = extractTimeseries(evt.dataTransfer.getData("text"));
		append2formula('"'+ts+'"');
	}

//-->
</script>

	<style type="text/css" id="holderjs-style"></style>
</head>

      <body onload="processMathJax();">
		<input type='hidden' id='conf'>
		<div id='mathml'><!--u class="spelling-error">error</u--></div>
        <form name="formulae" action="?" method="post">
         <input name="set_formulae" type="hidden"><input id="conf" type="hidden" value="">
         <!--input name="mml" value="set_formulae" type="hidden"--> <br>
         <div id="outputNode" style="display: none"></div>
		 <input name="inputText" id="inputText" type="hidden">	
         
         &nbsp;<textarea name="formula" id="formula" rows="3" cols="100%" onkeyup="checkKey(this, event)" placeholder='"timeseries"' alt='write here your formula, include timeseries in double quotes (")' title='write here your formula, include timeseries in double quotes (")'><?php echo $formula; ?></textarea>  <br><br>
		 <table> <tbody><tr valign="top"> <td>&nbsp;</td><td>
         <table style="border-collapse: separate;border-spacing: 2px;height: 305px;">
          <tbody>
          <tr>
			  <td colspan='6'>My formulae <?php personal_alarms(); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='?logout'>logout</a></td>
          </tr>
          <tr>
			  <td colspan='6'>Alarm NAME (max 15 chars) <input name='name' id='name' type='text' size='17' onkeyup='checkAlarmName()' onChange='checkAlarmName()' required value='<?php echo $name; ?>'></td>
          </tr>
          <tr valign='top'>
			  <td colspan='6'>details <textarea name='note' id='note' rows='3' cols="60%" placeholder='explain your alarm' ><?php echo $note; ?></textarea></td>
          </tr>
          <tr>
			  <td colspan='6'>Expiration date (YYYY-MM-DD) <input name='dateexpiration' id='dateexpiration' type='text' size='10' value='<?php echo (empty($dateexpiration)? date('Y-m-d', time()+86400*30): $dateexpiration); ?>' required></td>
          </tr>
          <tr valign='top'>
			  <td colspan='6'>&nbsp;</td>
          </tr>
          <!--tr> 
			  <td colspan='6'><input type='text' name='serach' id='search' size='53'>&nbsp;<button class="btn btn-primary" onClick='searchTs(); return false;'>search</button>&nbsp;</td>
		  </tr>
          <tr>
			  <td colspan='6'><select id='searchResults' size='11' style="width: 425px"></select></td>
          </tr>
          <tr> 
			  <td colspan='6'>similar&nbsp;<input type='checkbox' name='search_similar' id='search_similar'>&nbsp;&nbsp;
				  <button class="btn btn-primary" onClick='substituteTs(); return false;'>substitute</button>&nbsp;
				  <button class="btn btn-primary" onClick='appendTs(); return false;'>append</button>&nbsp;</td>			  
		  </tr>
          <tr>
			  <td colspan='6'>&nbsp;</td>
          </tr-->
		  <tr>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('7'); return false;" style="width:100%;font-weight:bold">7</button></td>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('8'); return false;" style="width:100%;font-weight:bold">8</button></td>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('9'); return false;" style="width:100%;font-weight:bold">9</button></td>
           <td style="width:68px;"> <button class="btn btn-primary" onClick="f(' / '); return false;" style="width:100%;font-weight:bold">/</button> </td>
           <td style="width:68px;"> <button class="btn btn-primary" onClick="f(' ( '); return false;" style="width:100%;font-weight:bold">(</button> </td>
           <td style="width:68px;"> <button class="btn btn-warning" onClick="f(' sin ( '); return false;" style="width:100%;font-weight:bold">sin</button> </td>
          </tr>
		  <tr>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('4'); return false;" style="width:100%;font-weight:bold">4</button> </td>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('5'); return false;" style="width:100%;font-weight:bold">5</button> </td>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('6'); return false;" style="width:100%;font-weight:bold">6</button> </td>
           <td style="width:68px;"> <button class="btn btn-primary" onClick="f(' * '); return false;" style="width:100%;font-weight:bold">X</button> </td>
           <td style="width:68px;"> <button class="btn btn-primary" onClick="f(' ) '); return false;" style="width:100%;font-weight:bold">)</button> </td>
           <td style="width:68px;"> <button class="btn btn-warning" onClick="f(' cos ( '); return false;" style="width:100%;font-weight:bold">cos</button> </td>
          </tr>
		  <tr>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('1'); return false;" style="width:100%;font-weight:bold">1</button> </td>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('2'); return false;" style="width:100%;font-weight:bold">2</button> </td>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('3'); return false;" style="width:100%;font-weight:bold">3</button> </td>
           <td style="width:68px;"> <button class="btn btn-primary" onClick="f(' - '); return false;" style="width:100%;font-weight:bold">-</button> </td>
           <td style="width:68px;"> <button class="btn btn-warning" onClick="f(' exp ( '); return false;" style="width:100%;font-weight:bold">exp</button> </td>
           <td style="width:68px;"> <button class="btn btn-warning" onClick="f(' tg ( '); return false;" style="width:100%;font-weight:bold">tg</button> </td>
          </tr>
		  <tr>
           <td colspan='2' style="width:140px;"> <button class="btn btn-info" onClick="f('0'); return false;" style="width:100%;font-weight:bold">0</button> </td>
           <td style="width:68px;"> <button class="btn btn-info" onClick="f('.'); return false;" style="width:100%;font-weight: bold;">.</button> </td>
           <td style="width:68px;"> <button class="btn btn-primary" onClick="f(' + '); return false;" style="width:100%;font-weight:bold">+</button> </td>
           <td style="width:68px;"> <button class="btn btn-warning" onClick="f(' log ( '); return false;" style="width:100%;font-weight:bold">log</button> </td>
           <td style="width:68px;"> <button class="btn btn-warning" onClick="f(' sqrt ( '); return false;" style="width:100%;font-weight:bold">sqrt</button> </td>
          </tr>
		  <tr>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' && '); return false;" style="width:100%;font-weight:bold">&amp;&amp;</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' || '); return false;" style="width:100%;font-weight:bold">||</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' max ( '); return false;" style="width:100%;font-weight:bold">max</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' min ( '); return false;" style="width:100%;font-weight:bold">min</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' abs '); return false;" style="width:100%;font-weight:bold">abs</button> </td>
           <td style="width:68px;"> <button class="btn btn-warning" onClick="f(' ^ '); return false;" style="width:100%;font-weight:bold">x<sup>y</sup></button> </td>
          </tr>
		  <tr>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' < '); return false;" style="width:100%;font-weight:bold">&lt;</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' <= '); return false;" style="width:100%;font-weight:bold">&le;</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' = '); return false;" style="width:100%;font-weight:bold">=</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' >= '); return false;" style="width:100%;font-weight:bold">&ge;</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' > '); return false;" style="width:100%;font-weight:bold">&gt;</button> </td>
           <td style="width:68px;"> <button class="btn btn-success" onClick="f(' != '); return false;" style="width:100%;font-weight:bold">&ne;</button> </td>
          </tr>
          </tbody></table>
			 </td><td>&nbsp;</td><td><div id="tree" style="width: 220px;height: 100%"> </div></td></tr>
			 <tr valign="top"><td colspan='10'><button class="btn btn-primary" onClick="exportFormula(); return false;" style="width:100%;font-size:180%;font-weight:bold">APPLY</button></td></tr>
		  </tbody></table>
        </form>

	<input type='hidden' id='conf'>
	<input type='hidden' id='ts'>
	<input type='hidden' id='start'>
	<input type='hidden' id='stop'>
	<input type='hidden' id='minY'>
	<input type='hidden' id='maxY'>
	<input type='hidden' id='logY'>
	<input type='hidden' id='height'>
	<input type='hidden' id='style'>

	<!-- Placed at the end of the document so the pages load faster -->
	<!-- fancytree -->
	<link href="./lib/fancytree-2.3.0/src/skin-lion/ui.fancytree.css" class="skinswitcher" rel="stylesheet" type="text/css">
	<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.js" type="text/javascript"></script>
	<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.dnd.js" type="text/javascript"></script>
	<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.table.js" type="text/javascript"></script>
	<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.columnview.js" type="text/javascript"></script>

	<!--script type="text/javascript" src="./MathJax.js?config=MML_HTMLorMML-full"></script-->
	<!--script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_HTMLorMML"></script-->
	<!--script type="text/javascript" src="./MathJax.js?config=AM_HTMLorMML-full"></script-->
	<script type="text/javascript" src="./lib/ASCIIMathML/ASCIIMathML.js"></script>
	<script type="text/javascript" src="./lib/ASCIIMathML/ASCIIMathMLeditor.js"></script>

	<script src='./conf.js?<?php echo time();?>' type="text/javascript"></script>
	<script src='./tree.js?<?php echo time();?>' type="text/javascript"></script>

	<script type="text/javascript">
	var dropFormula = document.getElementById('formula');
	dropFormula.addEventListener('dragover', handleDragOver, false);
	dropFormula.addEventListener('drop', handleFormulaSelect, false);
	</script>

</body></html>

