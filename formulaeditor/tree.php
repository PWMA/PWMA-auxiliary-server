<?php
	$domains = array(
		'ELETTRA'   => "tom.ecs.elettra.trieste.it:20000",
		'FERMI'     => "srv-tango-srf.fcs.elettra.trieste.it:20000",
		'PADReS'    => "srv-padres-srf.fcs.elettra.trieste.it:20000",
		'TIMER'     => "srv-tmr-srf.fcs.elettra.trieste.it:20000",
		'TIMEX'     => "srv-tmx-srf.fcs.elettra.trieste.it:20000",
		'LDM'       => "srv-ldm-srf.fcs.elettra.trieste.it:20000",
		'DiProI'    => "srv-diproi-srf.fcs.elettra.trieste.it:20000",
        'Magnedyn'  => "srv-mag-srf.fcs.elettra.trieste.it:20000",
        'TeraFERMI' => "srv-tf-srf.fcs.elettra.trieste.it:20000"
	);

	$output = array();
	if (isset($_REQUEST["search"])) {
		// $url = "https://pc-bogani.elettra.eu:10443/systems/tango/".strip_tags(strtr($_REQUEST["search"], array('tango://'=>'')));
		// $url = "http://pc-bogani.elettra.eu/10080/cs/tango://".strip_tags(strtr($_REQUEST["search"], array('tango://'=>'')));
		$url = "http://pwma-dev.elettra.eu/10080/v1/cs/".strip_tags($_REQUEST["search"]);
	}
	else if (isset($_REQUEST["key"])) {
		$url = strip_tags($_REQUEST["key"]).'/*';
	}
	else {
		// $domain = "ken.elettra.trieste.it/20000/*";
		$domain = "tom.ecs.elettra.trieste.it/20000/*";
		$domain = "srv-padres-srf/20000/*";
		$domain = "srv-tango-srf/20000/*";
		// $url = "https://pc-bogani.elettra.eu:10443/systems/tango/$domain";
		foreach ($domains as $key=>$val) {
			$url = "http://pwma-dev.elettra.eu:10080/v1/cs/tango://$val";
					// http://pc-bogani.elettra.eu:10080/cs/tango://srv-tango-srf.fcs.elettra.trieste.it:20000/ec-cblm-kg09-01/procfs
			$output[] = array('title'=>$key, 'key'=>strtr($url, array('*'=>'')), "lazy"=>true, "folder"=>true);
		}
		header("Content-Type: application/json");
		die(json_encode($output));		
	}

	file_put_contents('log.txt', json_encode($_REQUEST).', url: '.$url."\r\n", FILE_APPEND);
	// create curl resource
	$ch = curl_init();

	// set url
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/wwwelettraeu.crt");
	curl_setopt($ch, CURLOPT_URL, $url);

	//return the transfer as a string
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	if (isset($_REQUEST["search"])) {
		header("Content-Type: application/json");
		echo curl_exec($ch);
		curl_close($ch);
		exit();
	}
	// $output contains the output string
	$input = json_decode(curl_exec($ch));
	if (is_array($input[0])) $input = $input[0];

	if (isset($_REQUEST['debug'])) {echo "<pre>";print_r($input);echo "</pre>";}
	foreach ($input as $i) {
		$branch = substr_count($url, '/')<10;
		$output[] = array('title'=>$i, 'key'=>strtr($url, array('*'=>'')).$i, "lazy"=>$branch, "folder"=>$branch);
	}
	header("Content-Type: application/json");
	echo json_encode($output);

	// close curl resource to free up system resources
	curl_close($ch);

?>