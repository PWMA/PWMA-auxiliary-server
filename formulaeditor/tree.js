// ------------
// tree.js
// ------------

	var treeService = './lib/service/tree_service.php?conf=';
	console.log('treeService', treeService);
	var globalVal;
	var globalLabel;

	// INIT
	var $_GET = getQueryParams(document.location.search);
	if (typeof($_GET['conf']) !== 'undefined') {
		document.getElementById('conf').value = $_GET['conf'];
		initConf($_GET['conf']);
	}
	else {
		initConf(false);
	}
	initTree($_GET);
	console.log('treeService', treeService);

	function popup(location, title, params) {
		this.popupWindow = null;
		this.open = function () {
			if (!this.popupWindow ) {	// has not yet been defined
				this.popupWindow = window.open(location, title, params);
			}
			else {   // has been defined
				if (!this.popupWindow .closed) {  // still open
					this.popupWindow .focus();
				}
				else {
					this.popupWindow = window.open(location, title, params);
				}
			}
		};
	}

	function extractTimeseries(dropData) {
		myproperty = dropData.split('</property>');
		mystring = myproperty[0].split('string>');
		myts = mystring[1].split('<');
		return myts[0];
	}

	function initTree($_GET) {
		if (!$('#tree').length) return;
		var source_url = treeService;
		$("#tree").fancytree({
			autoScroll: true,
			source: {
				url: source_url
			},
			lazyLoad: function(event, data) {
				var node = data.node;
				// Issue an ajax request to load child nodes
				data.result = {
					url: treeService,
					data: {key: node.key}
				}
				console.log('lazyLoad::data', data);
			},
			click: function(event, data) {
				console.log('click::data', data);
				if (data.targetType == 'icon' || data.targetType == 'title') {
					append2formula('"tango:'+data.node.key.split('/tango:')[1]+'"');
				}
				return true;// Allow default processing
			},
			clickFolderMode: 2,
			persist: true
		});
	}

	function getQueryParams(qs) {
		qs = qs.split("+").join(" ");
		var params = {},
				tokens,
				re = /[?&]?([^=]+)=([^&]*)/g;
		while (tokens = re.exec(qs)) {
			params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
		}
		return params;
	}

	function handleTreeSelect(evt) {
		console.log('handleTreeSelect');
		evt.stopPropagation();
		evt.preventDefault();
		attr = extractTimeseries(evt.dataTransfer.getData("text"));
		// attr = evt.dataTransfer.getData('application/taurus-device'); does this work with taurus?
		addTs = evt.dataTransfer.dropEffect=='copy';
		console.log('get', treeService+'&searchkey='+attr);
		$.get(treeService+'&searchkey='+attr, function(data) {
			if (typeof(data[0]) !== 'undefined') {
				// console.log(document.getElementById('ts').value);
				var ts = (addTs && document.getElementById('ts').value.length)? document.getElementById('ts').value+';'+data[0]: data[0];
				updateLink(ts);
				document.getElementById('ts').value = ts;
			}
		});
	}

	function handleDragOver(evt) {
			console.log(evt);
		evt.stopPropagation();
		evt.preventDefault();
		evt.dataTransfer.dropEffect = 'plot'; // Explicitly show this is a copy.
	}

	// Setup the dnd listeners.
	var dropZone = document.getElementById('formula');
	dropZone.addEventListener('dragover', handleDragOver, false);
	dropZone.addEventListener('drop', handleTreeSelect, false);

	function strtr(myString, replaceArray){
		var oldString ;
		for (var i in replaceArray) {
			oldString = '';
			while (myString !== oldString) {
				oldString = myString;
				myString = oldString.replace(i,replaceArray[i]);
			}
		}
		return myString;
	}

	if (typeof($_GET['formula']) !== 'undefined') {
		var myPopup = new popup('./formula_editor.html?conf='+$_GET['conf'], 'formula', 'width=670,height=800');
		myPopup.open();
	}
