<!DOCTYPE html>
<html lang="en"><head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="LZ">
	<link rel="icon" href="https://www.elettra.eu/favicon.png">

	<title>uJive</title>
	<style type="text/css">
		ul.fancytree-container {
			width: 100%;
			height: 92%;
			overflow: auto;
			position: relative;
		}
		input,textarea,select {
			background-color: white;
			color: black;
		}
	</style>

	<!-- Bootstrap core CSS -->
	<link href="./lib/bootstrap/bootstrap.css" rel="stylesheet">
	<!-- Bootstrap theme -->
	<link href="./lib/bootstrap/bootstrap-theme.css" rel="stylesheet">

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./lib/bootstrap/ie10-viewport-bug-workaround.js"></script>

	<!-- jquery -->
	<link rel="stylesheet" href="./lib/jquery/jquery-ui.min.css">
	<script src="./lib/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="./lib/jquery/jquery-ui.min.js" type="text/javascript"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="./lib/bootstrap/bootstrap.js"></script>
	<script src="./lib/bootstrap/docs.js"></script>

	<?php
	if (isset($_REQUEST['varDetail'])) {
		if (isset($_REQUEST['auto_refresh'])) {
			// echo "<meta http-equiv='refresh' content='2'>\n";
			echo "<script>\n document.location= 'https://pwma.elettra.eu/cordova/index.html?version=displayVar&var=".$_REQUEST['varDetail']."';\n</script>\n";
		}
		echo "<body onload='console.log(\"in iframe \",document.body.scrollHeight);parent.valuesSize(document.body.scrollHeight, \"{$_REQUEST['id']}\");'>";
		$offset = 'none';
		$length = 1;
		if (strpos($_REQUEST['varDetail'], '[')!==false) {
			$varDetail = explode('[', $_REQUEST['varDetail']);
			if (strpos($varDetail[1], '-')!==false) {
				list($offset, $length) = explode('-', trim($varDetail[1], "] \n"));
				$length -= $offset-1;
			}
			else {
				$offset = trim($varDetail[1], "] \n");
			}
			$_REQUEST['varDetail'] = $varDetail[0];
		}
		$egiga = '';
		if (!empty($_REQUEST['formula'])) {
			$url = "https://pwma.elettra.eu:10443/v1/formula/".strtr($_REQUEST['formula'], array(' '=>'%20'));
		}
		else {
			$hh = 8;
			$url = "https://pwma.elettra.eu:10443/v1/cs/{$_REQUEST['varDetail']}";
			if (strpos($_REQUEST['varDetail'],'tango://tom.ecs.elettra.trieste.it:20000/') !== false) {
				$conf = "elettra";
			}
			else if (strpos($_REQUEST['varDetail'],'tango://srv-tango-srf.fcs.elettra.trieste.it:20000/') !== false) {
				$conf = "fermi";
			}
			else if (strpos($_REQUEST['varDetail'],'tango://srv-padres-srf.fcs.elettra.trieste.it:20000/') !== false) {
				$conf = "padres";
			}
			else if (strpos($_REQUEST['varDetail'],'tango://infra-control-01.elettra.trieste.it:20000/') !== false) {
				$conf = "infrastructure";
				$hh = 24;
			}
			$varDetail = explode('/', $_REQUEST['varDetail']);
			$s = implode('/',array_slice($varDetail, 3, 4, true));
			$egiga = "Sorry this variable isn't archived on <a href='http://fcsproxy.elettra.eu/docs/egiga2m/egiga2m.html?conf=fermi_hdbpp' target='_blank'>eGiga2m</a>";
			if (file_get_contents("http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/redirect_service.php?conf=$conf&s=$s&isArchived")=='YES') {
				$egiga = "Check last $hh hours logs on <a href='http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/redirect_service.php?conf=$conf&start=last%20$hh%20hours&s=$s' target='_blank'>eGiga2m</a>";
			}
		}
		echo "<a href='$url' target='_blank'>$url</a><br>\n$egiga<pre>";
		$d = json_decode(file_get_contents($url), true);
		if (!empty($d['timestamp'])) {
			list($int, $dec) = explode('.', $d['timestamp']);
			$d['timestamp'] = date('Y-m-d H:i:s', $int).'.'.$dec;
		}
		if (is_numeric($offset) && is_array($d['value']) && count($d['value'])>$offset) {
			$d['value'] = array_slice($d['value'], $offset, $length, true);
		}
		print_r($d);
		die("</pre></body>");
	}
	require_once("./conf.php");
	$sql = open_db();	
	session_start();
	$_SESSION['nofilter'] = isset($_REQUEST['nofilter'])? 1: 0;
	$username = '';
	if (isset($_SESSION['token'])) {
		$yy = date('Y');
		$webtokenExpirationSeconds = 36000; // 10 hh
		$data = $sql->sql_secure("SELECT * FROM userlog_$yy WHERE webtoken=$1 AND EXTRACT(EPOCH FROM NOW()-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", array($_SESSION['token']));
		if (!empty($data)) {
			$username = $data[0]['username'];
			$d = $sql->sql_secure("INSERT INTO userscreenlog_$yy (username, screenid, date, ip, filename) VALUES ($1, 0, NOW(), $2, 'pwma_jive.php')", array($username, $_SERVER['REMOTE_ADDR']));
		}
	}
	$data = $sql->sql_secure("SELECT tree_filter_default FROM starter WHERE username=$1", array($username));
	$filter = '';
	if (!empty($data)) {
		$owner = $username;
		$filter = $data[0]['tree_filter_default'];
		if (strpos($filter, ':') !== false) list($owner, $filter) = explode(':', $filter);
		$data = $sql->sql_secure("SELECT content FROM tree_filter WHERE owner=$1 AND title=$2", array($owner, $filter));
		if (!empty($data)) {
			$backgroundColor = isset($_REQUEST['nofilter'])? 'lightgray': 'darkgreen';
			$param = isset($_REQUEST['nofilter'])? '': '?nofilter';
			$filter = "<span style='position: relative;bottom: -30px;float: right;margin: 5px;margin-right: 20px;z-index: 2; background-color: white; padding: 1px 0px 1px 5px; cursor: pointer;' onclick=\"document.location = 'https://pwma.elettra.eu/pwma_jive.php$param';\">filter 
							<span style='background-color: $backgroundColor; color: white; padding: 5px; font-weight: bold; border-radius: 10px;'>$filter</span>
						</span>\n";
		}
	}
	if (!empty($_REQUEST['treefilter'])) {
		$data = $sql->sql_secure("SELECT content FROM tree_filter WHERE title=$1", array($_REQUEST['treefilter']),1);
		if (!empty($data)) {
			$backgroundColor = isset($_REQUEST['nofilter'])? 'lightgray': 'darkgreen';
			$param = isset($_REQUEST['nofilter'])? '': '?nofilter';
			$filter = "<span style='position: relative;bottom: -30px;float: right;margin: 5px;margin-right: 20px;z-index: 2; background-color: white; padding: 1px 0px 1px 5px; cursor: pointer;' onclick=\"document.location = 'https://pwma.elettra.eu/pwma_jive.php$param';\">filter 
							<span style='background-color: $backgroundColor; color: white; padding: 5px; font-weight: bold; border-radius: 10px;'>{$_REQUEST['treefilter']}</span>
						</span>\n";
		}
	}
	?>


	<script type="text/javascript">
		<!-- hide from non JavaScript browsers ->
			// INIT

		var formula_edit = true;
		var idIndex = 0;
		var refresh = [];
		function switchRefresh(idIndex, src) {
			refresh[idIndex] = !refresh[idIndex];
			if (refresh[idIndex]) {
				$('#reload'+idIndex).hide();
				$('#auto_refresh'+idIndex).attr('src','./lib/img/auto_refresh_on.png');
				$('#frame'+idIndex).attr('src', src+'&auto_refresh');
			}
			else {
				$('#reload'+idIndex).show();
				$('#auto_refresh'+idIndex).attr('src','./lib/img/auto_refresh_off.png');
				$('#frame'+idIndex).attr('src', src);
			}
		}
		function append2formula(append, formulaName, attrType) {
			var src;
			var height='';
			if (typeof formulaName == 'undefined') {
				// console.log('attrType: ', attrType);
				src = 'https://pwma.elettra.eu/pwma_jive.php?varDetail='+append.replace(/"/g, "");
				if (typeof attrType != 'undefined' && attrType == 'commands') {
					src = 'https://pwma.elettra.eu/cmd_prompt.php?varDetail='+append.replace(/"/g, "")+'&type='+attrType;
					height='height: 25em;';
				}
			}
			else {
				console.log('FORMULA: ', formulaName);
				src = 'https://pwma.elettra.eu/pwma_jive.php?formula='+formulaName+'&varDetail='+append.replace(/"/g, "");
			}
			console.log('src: ', src);
			if ($(window).width()<735) {
				$('#valuesModal').attr("src",src);
				$('#detailModal').show();
				$('#values').hide();
			}
			else {
				// $('#values').attr("src",src).show();
				idIndex++;
				refresh[idIndex] = false;
				src = src+'&id=id'+idIndex;
				// <button class='btn btn-info' onClick=\"$('#id"+idIndex+"').hide();\">close</button>
				var buttons = "<img src='./lib/img/close.png' style='width:16px; position: relative; bottom: -30px; float: right; margin: 5px; margin-right: 20px;' title='close' onClick=\"$('#id"+idIndex+"').remove();\">"
				if (typeof attrType == 'undefined' || attrType != 'commands') {
					buttons = buttons + "<img src='./lib/img/auto_refresh_off.png' style='width:16px; position: relative; bottom: -30px; float: right; margin: 5px;' title='auto refresh' id='auto_refresh"+idIndex+"' onClick=\"switchRefresh("+idIndex+",'"+src+"')\">";
					buttons = buttons + "<img src='./lib/img/reload.png' style='width:16px; position: relative; bottom: -30px; float: right; margin: 5px;' title='reload' id='reload"+idIndex+"' onClick=\"$('#frame"+idIndex+"').attr('src','"+src+"');\">";
				}
				$('#values').prepend("<div id='id"+idIndex+"' style='width: 100%;"+height+"'>"+buttons+"<iframe id='frame"+idIndex+"' src='"+src+"' style='width: 100%;height: 100%;'></div>");
			}
		}

		//-->
	</script>

	<style type="text/css" id="holderjs-style"></style>
	</head>

	<body>
		<nav class="navbar navbar-default" style='margin-bottom: 0px'>
			<div class="container-fluid bg-info">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>                        
					</button>
					<a href="http://pwma.elettra.eu/" border="0"><img src="https://pwma.elettra.eu/img/logo.png" class="media-object" style="width:40px"></a>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li class="active" style="padding-left: 10px; font-size: 180%; font-weight: bolder;">&mu;Jive</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<form class="navbar-form navbar-right" method="post" action="?"><?php echo $login; ?>
							</form>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<table style="width: 100%;height: 100%">
			<tr style="vertical-align: top;">
				<td style="width: 40%;height: 100%">
					<div id="tree" style="width: 100%">
						<?php echo $filter; ?>
					</div>
				</td>
				<td style="width: 60%;height: 100%"><div id='values' style="width: 100%; height: 100%;"></div></td>
			</tr>
		</table>
		<div id="detailModal" style="display: none; z-index: 1; position: absolute; right: 0px; top: 0px; background-color: #000080; width: 100%; height: 100%; padding: 0px; color: white; ">
			<button class="btn btn-info" onClick="$('#detailModal').hide();">close</button>
			<iframe id='valuesModal' style="width: 100%;height: 100%"></iframe>
		</div>
		<input type='hidden' id='formula'>

		<!-- Placed at the end of the document so the pages load faster -->
		<!-- fancytree -->
		<link href="./lib/fancytree-2.3.0/src/skin-lion/ui.fancytree.css" class="skinswitcher" rel="stylesheet" type="text/css">
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.dnd.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.table.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.columnview.js" type="text/javascript"></script>

		<script type="text/javascript">
			$('#tree').css('height', (window.innerHeight-0)+'px');
			$('#values').css('height', (window.innerHeight-0)+'px');
			function valuesSize(pixels, id) {
				console.log('pixels: ',pixels, ' - id: ', id);
				$('#'+id).css('height', (pixels+24)+'px');
			}
		</script>
		<script src='./lib/conf_jive.js?<?php echo time();?>' type="text/javascript"></script>
		<script src='./formulaeditor/tree.js?<?php if (!empty($_REQUEST['var'])) echo "var={$_REQUEST['var']}&"; echo time();?>' type="text/javascript"></script>

	</body>
</html>
