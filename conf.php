<?php
	define("HOST", "127.0.0.1");
	define("USERNAME", "your_username");
	define("PASSWORD", "your_password");
	define("DB", "your_db");
	$dbtype = "pg";

	$old_error_reporting = error_reporting(E_ALL);
	require_once("/var/www/html/lib/sql_interface.php");
	error_reporting($old_error_reporting);

	// ----------------------------------------------------------------
	// open_db
	function open_db() {
		global $dbtype;
		// instance SQL interface class
		$sql = new SqlInterface($dbtype);
		// connect to database
		$db = $sql->sql_connect(HOST, USERNAME, PASSWORD, DB);
		// if connection has failed emit a warnig and auto-retry
		if ($db === FALSE) {
			echo "<html>\n <head>";
			echo "	<meta http-equiv='refresh' content='5;url=#'>\n";
			echo " </head>\n <body>\n	<H1>Please wait</H1>Waiting for database connection<br />\n";
			exit();
		}
		// select db
		if ($dbtype!="pg") {$sql->sql_select_db(DB, $db);}
		return $sql;
	}
?>
