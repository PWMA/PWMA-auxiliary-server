<?php
	// echo "\$_SESSION<pre>"; print_r($_SESSION); echo "</pre>\n"; echo "\$_REQUEST<pre>"; print_r($_REQUEST); echo "</pre>\n";
	require_once("./conf.php");
	$username = '';
	$sql = open_db();	
	$yy = date('Y');
	if (isset($_REQUEST['tree'])) {
		// $mobileTree = '[{"title":"available sreens","data":[{"title":"FERMI","data":[{"title":"FERMI Status","component":"FermiStatus"},{"title":"PSS Hardware","component":"pssHardware","param":"fermi"}]},{"title":"Elettra","data":[{"title":"Elettra Status","component":"ElettraStatus"},{"title":"PSS Hardware","component":"pssHardware"}]}]}]';
		// $mobileTree = '[{"title":"FERMI","data":[{"title":"FERMI Status","component":"fermiStatus"},{"title":"PSS Hardware","component":"pssHardware","param":"fermi"}]},{"title":"Elettra","data":[{"title":"Elettra Status","component":"elettraStatus"},{"title":"PSS Hardware","component":"pssHardware"}]}]';
		$mobileTree = '[{"title":"FERMI","data":[{"title":"FERMI Status","component":"fermiStatus"}]},{"title":"Elettra","data":[{"title":"Elettra Status","component":"elettraStatus"}]}]';
		$data = $sql->sql_secure("SELECT screens FROM starter WHERE username = $1", array(isset($_REQUEST['username'])? $_REQUEST['username']: $username));
		if (!empty($data)) $mobileTree = $data[0]['screens'];
		// header("Content-Type: application/json"); die($mobileTree);
		if (isset($_REQUEST['mobile'])) {header("Content-Type: application/json"); die($mobileTree);}
		$webTree = strtr($mobileTree, array(',"data"'=>',"folder":true,"lazy":false,"expanded":false,"children"', ',"component"'=>',"folder":false,"lazy":false,"key"'));
		if (isset($_REQUEST['edit'])) {
			$webTree = strtr($webTree, array('"title":"'=>'"title":"<img src=\'./img/up.png\'>&nbsp;<img src=\'./img/plus.png\'>&nbsp;<img src=\'./img/down.png\'>&nbsp;','"expanded":false'=>'"expanded":true'));
		}
		header("Content-Type: application/json"); die($webTree);
		$jdec = json_decode($mobileTree);
		if (empty($jdec)) echo "empty(\$jdec)";
		echo "<pre>"; print_r($jdec); echo "</pre>";
		die('tree');
	}

	// ----------------------------------------------------------------
	// emit header
	function emit_header($title) {
		global $sql, $username, $yy;
		if (isset($_SESSION['token'])) {
			$webtokenExpirationSeconds = 28800; // 8 hh
			$data = $sql->sql_secure("SELECT * FROM userlog_$yy WHERE webtoken=$1 AND EXTRACT(EPOCH FROM NOW()-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", array($_SESSION['token']));
			$username = $data[0]['username'];
		}
		if (isset($_REQUEST['edit_node'])) {
			edit_node();
		}
		$login = '
			<div id="upper-form" style="display: none">
				<div class="form-group">
				  username <input type="text" name="username" class="form-control username" placeholder="name.surname">
				</div>
				<div class="form-group">
				  password <input type="password" name="elettra_ldap_password" class="form-control" placeholder="password">
				</div>
				<button type="submit" name="login" class="btn btn-default">Login</button>
			</div>';
		$login .= "
	<script>
		function openLogin() {
			if (document.getElementById('lowerform').style.display == 'none') {
				document.getElementById('lowerform').style.display = 'inline';
			}
			else {
				document.getElementById('lowerform').style.display = 'none';
			}
		}
		document.body.style.fontSize = '2em';
		if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)!=null && window.innerWidth>600) {
			document.body.style.fontSize = '5em';
		}
	</script>

		<div style='text-align:right; display: none' id='desktop-login'>
			<table><tr><td>
				<div class='form-group' style='margin: 10px'>
					<input type='text' name='du' class='form-control username' placeholder='name.surname'>
				</div></td><td>
				<div class='form-group' style='margin: 10px'>
					<input type='password' name='delp' class='form-control' placeholder='password'>
				</div></td><td>
				<button type='submit' name='dlogin' class='btn btn-default' style='margin: 10px'>Login</button>
			</td></tr></table>
		</div>
		<div style='text-align:right;' id='mobile-login'>
			<a class='mx-1 text-white' onClick='openLogin()' style='cursor: pointer; text-decoration: none;font-size: 1.2em;'>Login</a>
		</div>
		<script>
			var isMobile = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/);
			if (!isMobile && window.innerWidth > 750) {
				$('#mobile-login').hide();
				$('#desktop-login').show();
			}
		</script>
		<table id='lowerform' style='display: none; font-size: 1em;'>
			<tr style='vertical-align: middle;' id='username'>
				<td>Username</td><td align='right'><input type='text' style='width: 10em;font-size: 1em;height: auto;' name='username' class='form-control username' placeholder='name.surname'></td>
			</tr>
			<tr style='vertical-align: middle;' id='password'>
				<td>Password </td><td align='right'><input type='password' style='margin-top: 4px;width: 10em; font-size: 1em;height: auto;' name='elettra_ldap_password' class='form-control' placeholder='password'></td> 
			</tr>
			<tr style='vertical-align: middle;' id='submit'>
				<td>&nbsp;</td><td align='right'>&nbsp;<input class='btn btn-default' type='submit' style='margin-top: 4px; width: 10em; font-size: 1em;height: auto;align: right' value='login'></div> 
			</tr>
		</table>
		\n";
		if (strlen($username)>0) $login = "welcome $username <button type='submit' name='logout' class='btn btn-default'>Logout</button>	<script>
		document.body.style.fontSize = '2em';
		if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/)!=null && window.innerWidth>600) {
			document.body.style.fontSize = '5em';
		}
	</script>
";
/*
	<nav class="navbar navbar-default">
	  <div class="container-fluid bg-info">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>                        
		  </button>
		  <a class="navbar-brand" href="http://pwma.elettra.eu/"><img src="https://pwma.elettra.eu/img/logo.png" style="width:40px; height:40px; margin-top: -10px; margin-left: -10px;"> <span style="font-size: 150%; font-weight: bolder; color: black">Pwma web</span></a>
		  <!--a href="http://pwma.elettra.eu/" border="0"><img src="https://pwma.elettra.eu/img/logo.png" class="media-object" style="width:40px"></a-->
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
		  <!--ul class="nav navbar-nav">
			<li class="active" style="padding-left: 10px; font-size: 180%; font-weight: bolder;">Pwma web</li>
		  </ul-->
		  <ul class="nav navbar-nav navbar-right">
			 <li>
			  <form class="navbar-form navbar-right" method="post" action="?">'.$login.'
			  </form>
			 </li>
		  </ul>
		</div>
	  </div>
	</nav>
*/
		echo '
	<link rel="stylesheet" href="./lib/jquery/jquery-ui.min.css">
	<link rel="icon" href="./img/elettra.png">

	<!-- Bootstrap core CSS -->
	<link href="./lib/bootstrap/bootstrap.css" rel="stylesheet">
	<!-- Bootstrap theme -->
	<link href="./lib/bootstrap/bootstrap-theme.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="./lib/bootstrap/theme.css" rel="stylesheet">

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./lib/bootstrap/ie10-viewport-bug-workaround.js"></script>
	<link type="text/css" rel="stylesheet" href="css/index.css" />

	<script src="./lib/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="./lib/jquery/jquery-ui.min.js" type="text/javascript"></script>
	<!-- Bootstrap core JavaScript -->
	<script src="./lib/bootstrap/bootstrap.js"></script>
	<!-- script src="./lib/bootstrap/docs.js"></script-->

  <!-- link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script-->

	<body style="padding: 0px; margin: 0px; font-size: 4em;">
	<nav class="navbar navbar-default">
	  <div class="container-fluid bg-info">
		<div class="navbar-header" style="width: 100%">
		  <a class="navbar-brand" href="http://pwma.elettra.eu/"><img src="https://pwma.elettra.eu/img/logo.png" style="width:40px; height:40px; margin-top: -10px; margin-left: -10px;"> 
		  <span style="font-size: 150%; font-weight: bolder; color: black">PWMA</span-->
		  <!--h2 style="color: black">PWMA</h2--></a>
		  <ul style="float: right"><!--class="nav navbar-form navbar-right"-->
			 <div>
			  <form method="post" action="?" style="margin: 0px;">'.$login.'
			  </form>
			 </div>
		  </ul>
		</div>
	  </div>
	</nav>
';
	echo "<br>";
		if (empty($username) && empty($_REQUEST['token'])) {
			// Prepare list of users
			$jsSearch = "	<script type=\"text/javascript\">\n	var userList = [\n";
			$f = file('http://fcsproxy.elettra.trieste.it/docs/pss/users.csv');	
			foreach ($f as $l) {$jsSearch .= "'".trim($l)."',\n";}
			$jsSearch .= "''";
			$jsSearch .= "];\n$( function() {\n $( \".username\" ).autocomplete({source: userList, select: function( event, ui ) {console.log(''/*, ui.item.value*/);}});})\n	</script>\n";
			echo $jsSearch;
			die('<div style="text-align: center"><a href="http://pwma.elettra.eu/cordova/index.html?version=FermiStatus" class="button">FERMI Status</a><br><br><br> 
				<a href="http://pwma.elettra.eu/cordova/index.html?version=ElettraStatus" class="button">Elettra Status</a><br><br><br> 
				<a href="./apk/" class="button">get PWMA Mobile</a></div>');
		}
	}

	// ----------------------------------------------------------------
	// tree graphic editor popup
	function edit_node() {
		global $username, $sql;
		$title = $_REQUEST['title'];
		$treeuser = isset($_REQUEST['treeuser'])? $_REQUEST['treeuser']: $username; 
		$component = empty($_REQUEST['component'])? '': "<tr><td>component </td><td><input id='component' size='80' value=\"{$_REQUEST['component']}\"></td></tr>\n";
		$param = empty($_REQUEST['param'])? '': "<tr><td>parameter </td><td><input id='component' size='80' value=\"{$_REQUEST['param']}\"></td></tr>\n";
		$data = $sql->sql_secure("SELECT id,title FROM screen ORDER BY title", array());
		$userscreens = '';
		if (!empty($data)) {
			$userscreens .= "<select id='userScreens' onChange=\"el = document.getElementById('userScreens'); document.getElementById('newcomponent').value='pwma://userscreen'; document.getElementById('newparam').value=el.children[el.selectedIndex].value; document.getElementById('newtitle').value=el.children[el.selectedIndex].innerHTML.trim();\"><option value=''></option>\n";
			foreach ($data as $d) {
				$userscreens .= "<option value='{$d['id']}'>{$d['title']}</option>\n";
			}
			$userscreens .= "</select>\n";
		}
		echo "
			<style type='text/css' media='screen'>
				input,textarea,select {
					background-color: white;
					color: black;
				}
				button {
					background-color: LightGray;
					color: black;
				}
			</style>
			<h3>edit node: $title</h3>
			<script>
				function edit() {
					if (document.getElementById('param')) {
						res = window.opener.editComponent('edit', \"$title\", document.getElementById('title').value, {old: \"{$_REQUEST['component']}\", new: document.getElementById('component').value}, {old: \"{$_REQUEST['param']}\", new: document.getElementById('param').value});
					}
					else if (document.getElementById('component')) {
						res = window.opener.editComponent('edit', \"$title\", document.getElementById('title').value, {old: \"{$_REQUEST['component']}\", new: document.getElementById('component').value});
					}
					else {
						res = window.opener.editComponent('edit', \"$title\", document.getElementById('title').value);
					}
					if (res > -1) window.close();
				}
				function addComponent() {
					res = window.opener.editComponent('new', \"$title\", document.getElementById('newtitle').value, document.getElementById('newcomponent').value, document.getElementById('newparam').value);
					if (res > -1) window.close();
				}
				function addFolder() {
					res = window.opener.editComponent('newFolder', \"$title\", document.getElementById('newfolder').value);
					if (res > -1) window.close();
				}
			</script>
			<button onClick='window.opener.editComponent(\"remove\", \"$title\"); window.close();'>remove</button><hr>
			<table><tr><td>title</td><td><input id='title' value=\"$title\"></td></tr>
				$component$param
				<tr><td colspan='2'><button onClick='edit();'>edit</button></td>
			</table><hr>
			<table><tr><td>title</td><td><input id='newtitle'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;userscreens: $userscreens</td></tr>
				<tr><td>component </td><td><input id='newcomponent' size='80'></td></tr>
				<tr><td>parameter </td><td><input id='newparam' size='80'></td></tr>
				<td colspan='2'><button onClick='addComponent()'>append new component</button></td></tr>
			</table><hr>
			<table><tr><td>title</td><td><input id='newfolder'></td></tr>
				<tr><td colspan='2'><button onClick='addFolder()'>append folder</button></td></tr>
			</table><hr>
			<button onClick='window.close()'>cancel</button><br><br>
			\n";
		exit();
	}

	// ----------------------------------------------------------------
	// JSON pretty print https://stackoverflow.com/questions/6054033/pretty-printing-json-with-php
	function prettyPrint($json) {
		$result = '';
		$level = 0;
		$in_quotes = false;
		$in_escape = false;
		$ends_line_level = NULL;
		$json_length = strlen( $json );
		$close_graph = false;
		$next_end = false;

		for( $i = 0; $i < $json_length; $i++ ) {
			$char = $json[$i];
			$new_line_level = NULL;
			$post = "";
			if( $ends_line_level !== NULL ) {
				$new_line_level = $ends_line_level;
				$ends_line_level = NULL;
			}
			if ($next_end) {
				$new_line_level = $level;
				$next_end = false;
			}
			if ( $in_escape ) {
				$in_escape = false;
			} else if( $char === '"' ) {
				$in_quotes = !$in_quotes;
			} else if( ! $in_quotes ) {
				switch( $char ) {
					case ']':
						$level--;
						$ends_line_level = NULL;
						$new_line_level = $level;
						break;

					case '}':
						$close_graph = true;
						break;

					case ',':
						if ($close_graph) {
							$close_graph = false;
							$ends_line_level = NULL;
							$next_end = true;
						}
						else {
							$post = " ";
						}
						break;

					case '[':
						$level++;
						$ends_line_level = $level;
						break;

					case ':':
						$post = " ";
						break;

					case " ": case "\t": case "\n": case "\r":
						$char = "";
						$ends_line_level = $new_line_level;
						$new_line_level = NULL;
						break;
				}
			} else if ( $char === '\\' ) {
				$in_escape = true;
			}
			if( $new_line_level !== NULL ) {
				$result .= "\n".str_repeat( "\t", $new_line_level );
			}
			$result .= $char.$post;
		}

		return $result;
	}

	// ----------------------------------------------------------------
	// screen text editor
	function screen_text_editor($screen) {
		global $sql, $username, $yy;
		if (isset($_REQUEST['save'])) {
			$data = $sql->sql_secure("UPDATE screen SET content=$1,last_update=NOW() WHERE id=$2 AND username=$3", array(strtr($_REQUEST['screendata'], array("\t"=>'', '\n'=>'', ': '=>':', ', '=>',')), $_REQUEST['screen_text_editor'], $username));
		}
		$data = $sql->sql_secure("SELECT * FROM screen ORDER BY username, title", array());
		$select = "<select id='screen' onChange=\"document.location = 'pwma_starter.php?screen_text_editor='+$('#screen').val()\">\n<option value=''></option>\n";
		$screen_data = $save = '';
		$save_as = "<input type='submit' class='btn btn-secondary' name='save_as' value='Save as'> <input type='text' name='newname'>&nbsp;&nbsp;";
		foreach ($data as $d) {
			$selected = '';
			if (!empty($screen) && ($screen==$d['id'])) {
				$selected = ' SELECTED';
				$screen_data = prettyPrint($d['content']);
				if ($d['username']==$username) $save = "<input type='submit' class='btn btn-secondary' name='save' value='Save'>&nbsp;&nbsp;";
			}
			$select .= "<option value='{$d['id']}'$selected>{$d['username']} - {$d['title']}</option>\n";
		}
		echo "$select</select><br><br>\n<form method='post' action='?screen_text_editor=$screen'>$save$save_as<br><br><textarea name='screendata' id='screendata' rows='40' style='width: 100%'>$screen_data</textarea></form><br><br>\n";
		die('');
	}

	// ----------------------------------------------------------------
	// tree graphic editor
	function tree_geditor() {
		global $sql, $username, $yy;
		$treeuser = isset($_REQUEST['treeuser'])? $_REQUEST['treeuser']: $username; 
		$data = $sql->sql_secure("SELECT screens FROM starter WHERE username = $1", array($username));
		$webTree = strtr($data[0]['screens'], array(',"data"'=>',"folder":true,"lazy":false,"expanded":false,"children"', ',"component"'=>',"folder":false,"lazy":false,"key"'));
		$webTree = strtr($webTree, array('"title":"'=>'"title":"<img src=\'./img/up.png\'>&nbsp;<img src=\'./img/plus.png\'>&nbsp;<img src=\'./img/down.png\'>&nbsp;','"expanded":false'=>'"expanded":true'));
		if (isset($_REQUEST['text'])) {
			if (isset($_REQUEST['save'])) {
				$data = $sql->sql_secure("UPDATE starter SET screens = $1, datemodified=NOW() WHERE username = $2", array($_REQUEST['treedata'], $treeuser));
				echo $sql->sql_error()."<br>\n";
				$data = $sql->sql_secure("INSERT INTO starterLog_$yy (screens, datemodified, username) VALUES ($1, NOW(), $2)", array($_REQUEST['treedata'], $username));
				echo $sql->sql_error()."<br>\n";
				$data = $sql->sql_secure("SELECT screens FROM starter WHERE username = $1", array($username));
			}
			$starter_tree = prettyPrint('[{"title":"FERMI","data":[{"title":"FERMI Status","component":"FermiStatus"}]},{"title":"Elettra","data":[{"title":"Elettra Status","component":"ElettraStatus"}]}]');
			if (!empty($data[0]['screens'])) $starter_tree= $data[0]['screens'];
			echo "<form method='post' action='?tree_geditor&text'>
				<input type='submit' class='btn btn-secondary' name='save' value='Save'>&nbsp;&nbsp;
				<input type='submit' class='btn btn-secondary' onClick=\"window.location='pwma_starter.php?tree_geditor'; return false;\" value='Graphic Starter Editor'>&nbsp;&nbsp;
				<input type='submit' class='btn btn-secondary' onClick=\"window.location='pwma_starter.php'; return false;\" value='Exit'><br><br>
				<textarea name='treedata' id='treedata' rows='20' cols='120'>$starter_tree</textarea><br><br>
				</form>\n";
		}
		else {
			echo '
		<style type="text/css" media="screen">
			button {
				background-color: LightGray;
				color: black;
			}
		</style>
		<button onClick="saveTree()" class="btn btn-secondary">Save</button>&nbsp;&nbsp; <button onClick="window.location=\'pwma_starter.php?tree_geditor&text\'" class="btn btn-secondary">Text Starter Editor</button>&nbsp;&nbsp; <button onClick="window.location=\'pwma_starter.php\'" class="btn btn-secondary">Exit</button>
		<form id="treeForm" method="post" action="./pwma_starter.php?gsave"><textarea name="treeSaved" id="treeSaved" style="display:none"></textarea></form><br>
		<div id="tree"></div><br>
		<!-- Placed at the end of the document so the pages load faster -->
		<!-- fancytree -->
		<link href="./lib/fancytree-2.3.0/src/skin-pwma/ui.fancytree.css" class="skinswitcher" rel="stylesheet" type="text/css">
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.dnd.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.table.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.columnview.js" type="text/javascript"></script>'."\n<script>
		var treeService = './pwma_starter.php?tree&edit&username=$username';
		var treeData = `{$webTree}`;
		var globalVal;
		var globalLabel;
		var myWindow;

		// INIT
		var \$_GET = getQueryParams(document.location.search);
		initTree(\$_GET);
		console.log('treeData', treeData);

		function popup(location, title, params) {
			newwindow=window.open(location,title,'height=800,width=400');
			if (window.focus) {newwindow.focus()}
			return false;
		}

		function saveTree() {
			document.getElementById('treeSaved').innerHTML = treeData;
			document.getElementById('treeForm').submit();
			return false;
		}

		function editComponent(action, oldTitle, newTitle, newComponent, newParam) {
			const tokenPos = treeData.indexOf(`&nbsp;\${oldTitle}\"`);
			const tokenStart = treeData.lastIndexOf('\\n',tokenPos) + 1;
			const tabNum = treeData.lastIndexOf('{',tokenPos) - tokenStart;
			const closeGr = treeData.indexOf('}', tokenStart);
			let tokenStop = closeGr + 1;
			let tabs = '';
			for (i=0; i<tabNum; i++) tabs = tabs+'\\t';
			const childrenStart = treeData.indexOf('\"children\":[', tokenStart)
			// if multi-line node (not leaf)
			if (closeGr > childrenStart && childrenStart > 0) {
				tokenStop = treeData.indexOf('\\n'+tabs+']}', tokenStart) + tabNum + 3;
			}
			if (action=='remove') {
				treeData = treeData.slice(0, tokenStart - (treeData[tokenStart-2]==','? 2: 1)) + treeData.slice(tokenStop);
			}
			if (action=='edit') {
				if (newTitle!==oldTitle && treeData.indexOf(`&nbsp;\${newTitle}\"`)>-1) {alert('ERROR, duplicate Title: '+newTitle); return -1;}
				let comp = treeData.slice(tokenStart, tokenStop).replace(`&nbsp;\${oldTitle}\"`,`&nbsp;\${newTitle}\"`);
				if (newComponent) comp = comp.replace(`\"key\":\"\${newComponent.old}\"`, `\"key\":\"\${newComponent.new}\"`);
				if (newParam) comp = comp.replace(`\"param\":\"\${newParam.old}\"`, `\"param\":\"\${newParam.new}\"`);
				treeData = treeData.slice(0, tokenStart) + comp + treeData.slice(tokenStop);
				console.log(treeData);
			}
			else if (action=='new') {
				if (treeData.indexOf(`&nbsp;\${newTitle}\"`)>-1) {alert('ERROR, duplicate Title: '+newTitle); return -1;}
				treeData = treeData.slice(0, tokenStop) + ',\\n' + tabs + '{\"title\":\"' + \"<img src='./img/up.png'>&nbsp;<img src='./img/plus.png'>&nbsp;<img src='./img/down.png'>&nbsp;\" + newTitle + '\",\"folder\":false,\"lazy\":false,\"key\":\"' + newComponent + '\",\"param\":\"' + newParam + '\"}' + treeData.slice(tokenStop);
			}
			else if (action=='newFolder') {
				if (treeData.indexOf(`&nbsp;\${newTitle}\"`)>-1) {alert('ERROR, duplicate Title: '+newTitle); return -1;}
				i=1; while (treeData.indexOf(`&nbsp;new title \${i}\"`)>-1 && i<100) i++;
				treeData = treeData.slice(0, tokenStop) + ',\\n' + tabs + '{\"title\":\"' + \"<img src='./img/up.png'>&nbsp;<img src='./img/plus.png'>&nbsp;<img src='./img/down.png'>&nbsp;\" + newTitle + '\",\"folder\":true,\"lazy\":false,\"expanded\":true,\"children\":[\\n' + 
					tabs + '\\t{\"title\":\"'+ \"<img src='./img/up.png'>&nbsp;<img src='./img/plus.png'>&nbsp;<img src='./img/down.png'>&nbsp;\"+'new title '+i+'\",\"folder\":false,\"lazy\":false,\"key\":\"pwma://userscreen\",\"param\":\"1\"}\\n' + 
					tabs + ']}' + treeData.slice(tokenStop);
			}
			\$(\"#tree\").fancytree('option', 'source', JSON.parse(treeData));
			return 0;
		}

		function extractTimeseries(dropData) {
			myproperty = dropData.split('</property>');
			mystring = myproperty[0].split('string>');
			myts = mystring[1].split('<');
			return myts[0];
		}

		function getToken(token, position) {
			const tokenPos = treeData.indexOf(`\"\${token}\"`);
			const tokenStart = treeData.lastIndexOf('\\n',tokenPos) + 1;
			const tabNum = treeData.lastIndexOf('{',tokenPos) - tokenStart;
			const closeGr = treeData.indexOf('}', tokenStart);
			let tokenStop = closeGr + 1;
			const childrenStart = treeData.indexOf('\"children\":[', tokenStart)
			// if multi-line node (not leaf)
			if (closeGr > childrenStart && childrenStart > 0) {
				let tabs = '';
				for (i=0; i<tabNum; i++) tabs = tabs+'\\t';
				tokenStop = treeData.indexOf('\\n'+tabs+']}', tokenStart) + tabNum + 3;
			}
			if (position == 'this') {
				console.log('tokenPos,tokenStart, tabNum, stop', tokenPos, tokenStart, tabNum, tokenStop);
				return treeData.substring(tokenStart, tokenStop);
			}
			if (position == 'next') {
				if (treeData[tokenStop] == ',') {
					const nextStart = tokenStop + 2;
					const nextClose = treeData.indexOf('}', nextStart);
					let nextStop = nextClose + 1;
					// if multi-line node (not leaf)
					if (nextClose > treeData.indexOf('\"children\":[', nextStart)) {
						let tabs = '';
						for (i=0; i<tabNum; i++) tabs = tabs+'\\t';
						nextStop = treeData.indexOf('\\n'+tabs+']}', nextStart) + tabNum + 3;
					}
					return treeData.substring(nextStart, nextStop);
				}
				return '';
			}
			if (position == 'previous') {
				if (treeData[tokenStart-2] == ',') {
					let prevStart = treeData.lastIndexOf('\\n', tokenStart-2) + 1;
					// if multi-line node (not leaf)
					if (treeData[tokenStart-4] == ']') {
						const tn = tokenStart - prevStart - 4;
						let tabs = '';
						for (i=0; i<tn; i++) tabs = tabs+'\\t';
						prevStart = treeData.lastIndexOf('\\n'+tabs+'{', tokenStart-2) + 1;
					}
					return treeData.substring(prevStart, tokenStart-2);
				}
				return '';
			}
		}

		function replaceNodes(first, second) {
			let myData = treeData.replace(first, 'firstToken');
			myData = myData.replace(second, first);
			treeData = myData.replace('firstToken', second);
			console.log(treeData);
			\$(\"#tree\").fancytree('option', 'source', JSON.parse(treeData));
		}

		function editNode(action, node) {
			const title = node.title;
			// console.log('action, title', action, title);
			if (action=='up') {
				previousToken = getToken(title, 'previous');
				thisToken = getToken(title, 'this');
				if (previousToken.length) {
					replaceNodes(previousToken, thisToken);
				}
			}
			if (action=='down') {
				thisToken = getToken(title, 'this');
				nextToken = getToken(title, 'next');
				if (nextToken.length) {
					replaceNodes(thisToken, nextToken);
				}
			}
			if (action=='edit') {
				const component = node.key && !node.folder? '&component='+node.key: '';
				const param = node.data.param? '&param='+node.data.param: '';
				console.log(myWindow);
				myWindow = window.open('?edit_node&title='+title.split('&nbsp;')[3]+component+param,'edittree','width=800,height=500');
			}
		}

		function initTree(\$_GET) {
			if (!\$('#tree').length) return;
			var source_url = treeService;
			\$(\"#tree\").fancytree({
				autoScroll: true,
				source: {
					url: source_url
				},
				click: function(event, data) {
					console.log('event, data.node',event, data.node);
					if (event.srcElement && event.srcElement.src) {
						if (event.srcElement.src.indexOf('up.png')>-1) {editNode('up', data.node); return false;}
						else if (event.srcElement.src.indexOf('down.png')>-1) {editNode('down', data.node); return false;}
						else if (event.srcElement.src.indexOf('plus.png')>-1) {editNode('edit', data.node); return false;}
					}
					else if (data.node.key.indexOf('_')!==0) {
						if (data.node.key.indexOf('http')===0) {
							window.location = data.node.key;
						}
						else {
							window.location = 'http://pwma.elettra.eu/cordova/index.html?version='+data.node.key;
						}
					}
					return true;// Allow default processing
				},
				clickFolderMode: 2,
				persist: true
			});
		}

		function getQueryParams(qs) {
			qs = qs.split(\"+\").join(\" \");
			var params = {},
					tokens,
					re = /[?&]?([^=]+)=([^&]*)/g;
			while (tokens = re.exec(qs)) {
				params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
			}
			return params;
		}
		</script>";
		}
		exit(0);
	}

/*

<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBh4vcAn16DYHTdpb_0kTfgVcXA2_NHoIs",
    authDomain: "pwma-7c60d.firebaseapp.com",
    databaseURL: "https://pwma-7c60d.firebaseio.com",
    projectId: "pwma-7c60d",
    storageBucket: "pwma-7c60d.appspot.com",
    messagingSenderId: "723966483617"
  };
  firebase.initializeApp(config);
</script>
*/


	// ----------------------------------------------------------------
	// alarm self administration
	function alarm_admin() {
		global $sql, $username;
		$alarm = empty($_REQUEST['alarm'])? '': "?myformula=".urlencode($_REQUEST['alarm']);
		if (isset($_REQUEST['token'])) {
			echo "<div style='margin: 10'><a href='./formulaeditor/formula_editor.php$alarm' class='button'>Alarm Editor</a><br><br>\n";
			$data = $sql->sql_secure("SELECT id FROM token WHERE value=$1", array($_REQUEST['token']));
			$tokenid = $data[0]['id'];
			echo "token: {$_REQUEST['token']}<br>\n";
			if (isset($_REQUEST['alarm'])) {
				echo '<b>'.strip_tags($_REQUEST['alarm']).'</b><br>';
				$token = $_REQUEST['token'];
				$data = $sql->sql_secure("SELECT * FROM formula WHERE label=$1", array($_REQUEST['alarm']));
				$formula = $data[0]['formula'];
				$splitFormula = preg_split( "/[ <>=+*|&()]/", $formula);
				// echo "<pre>"; print_r($splitFormula);echo "</pre>";
				$replace = array();
				foreach ($splitFormula as $tok) {
					if (strpos($tok, 'tango://')!==false) {
						$f = file_get_contents("https://pwma.elettra.eu:10443/v1/cs/$tok");
						if (!empty($f)) {
							list($trash, $val) = explode('"value":', trim($f,'}'));
							$replace[$tok] = "$tok [=<span style='color: green;font-weight: bold;'>$val</span>]\n";
						}
					}
				}
				$formula = strtr($formula, $replace);
				echo "formula: $formula<br>
					explanation: {$data[0]['note']}<br>
					date of creation: {$data[0]['datecreated']}<br>
					date of expiration: {$data[0]['dateexpiration']}<br>
					delay: {$data[0]['delay']}<br><br>
					<script>
					  function plotAlarm(formula) {
						const ftoken = formula.split(/[ <>=+*|&]/);
						console.log('ftoken', ftoken);
						const charts = [];
						let j = 0;
						let hh = 8;
						let conf = '';
						for (let i = 0; i < ftoken.length; i++) {
						  if (ftoken[i].indexOf(':20000/') !== -1) {
							const ft = ftoken[i].split(':20000/');
							charts[j] = ft[1];
							j += 1;
							if (ftoken[i].indexOf('tango://tom.ecs.elettra.trieste.it:20000/') !== -1) {
							  conf = 'elettra';
							}
							else if (ftoken[i].indexOf('tango://srv-tango-srf.fcs.elettra.trieste.it:20000/') !== -1) {
							  conf = 'fermi';
							}
							else if (ftoken[i].indexOf('tango://srv-padres-srf.fcs.elettra.trieste.it:20000/') !== -1) {
							  conf = 'padres';
							}
							else if (ftoken[i].indexOf('tango://infra-control-01.elettra.trieste.it:20000/') !== -1) {
							  conf = 'infrastructure';
							  hh = 48;
							}
						  }
						}
						const src = `http://ecsproxy.elettra.eu/docs/egiga2m/lib/service/redirect_service.php?conf=\${conf}&start=last%20${hh}%20hours&s=\${charts.join(';')}`;
						console.log('src', src);
						document.location = src;
					  }
					  function ackAlarm(alarm, token) {
						const url = 'https://pwma.elettra.eu:10443/v1';
						console.log('ackAlarm(alarm, token)', `\${url}/formula/\${alarm} {${token}}`);
						fetch(`\${url}/formula/\${alarm}`, {
						  method: 'POST',
						  headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							token: token,
						  },
						  body: JSON.stringify({ack: 1}),
						})
						.then(response => {
						  console.log('ackAlarm(alarm, token), response', response);
						})
					  }
					</script>
					<style type='text/css' media='screen'>
						button {
							background-color: LightGray;
							color: black;
						}
					</style>
					<a onClick=\"ackAlarm('{$_REQUEST['alarm']}', '$token');document.location = './pwma_starter.php?alarm_admin&alarm={$_REQUEST['alarm']}&token={$_REQUEST['token']}'\" class='button'>ACK</a>
					<a onClick=\"plotAlarm('{$data[0]['formula']}')\" class='button'>eGiga2m</a> only if in HDB(pp)
				<br>\n";
				$data = $sql->sql_secure("SELECT * FROM alarmlog WHERE tokenId=$1 AND label=$2 ORDER BY date DESC LIMIT 1000", array($tokenid,$_REQUEST['alarm']));
				echo "<br><br><table>\n<tr><td>date</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
				$old_value = '';
				if (!empty($data)) foreach ($data as $d) {
					$value = '';
					if ($d['subscribe']=='f') $value = 'not subscribed';
					else if ($d['ack']=='t') $value = 'OK';
					else if ($d['ack']=='f') $value = "<span style='color:red'>ALARM</span>";
					// don't show repeted values
					if (!empty($old_value) && $old_value != $value) echo "<tr><td> $old_date </td><td>&nbsp;</td><td> $old_value </td></tr>\n";
					$old_value = $value;
					$old_date = $d['date'];
				}
				echo "<tr><td> $old_date </td><td>&nbsp;</td><td> $old_value </td></tr>\n";
				die("</table>\n");
			}
			if (isset($_REQUEST['delete'])) {
				$data = $sql->sql_secure("DELETE FROM alarm WHERE tokenId=$1", array($tokenid));
				echo "Deleted all alarms of $username<br><br>\n";
			}
			$columns = 'label,formula,note,username,domain,system,dateexpiration,delay';
			$data = $sql->sql_secure("SELECT * FROM alarm WHERE tokenId=$1 ORDER BY label", array($tokenid));
			$labels = $alarm = array();
			$echoBuf = "<a href='?alarm_admin&delete&token={$_REQUEST['token']}'>Unsubscribe all Alarms</a><table>\n<tr><td colspan='2' style=\"font-size: 26; color: #3333ff; font-weight: bold\">Subscribed Alarms</td></tr>\n";
			if (!empty($data)) foreach ($data as $d) {
				$fdata = $sql->sql_secure("SELECT DATE(dateexpiration) AS date, dateexpiration<NOW() AS expired FROM formula WHERE label=$1", array($d['label']));
				$expired = $fdata[0]['expired']=='t'? "<img src='./img/alert.png' width='20px' title='Warning: formula has expired {$fdata[0]['date']}'>": '';
				$echoBuf .= "<tr style='height: 45px;' id='{$d['label']}'><td style='align: right;'>$expired {$d['label']}</td><td>&nbsp;<a href='?alarm_admin&unsubscribe=1&alarm={$d['label']}&token={$_REQUEST['token']}' class='button'>detail</a>&nbsp;
				<a onClick=\"unsubscribeAlarm('{$d['label']}', '{$_REQUEST['token']}');\" class='button'>remove</a>&nbsp;
				<a onClick=\"ackAlarm('{$d['label']}', '{$_REQUEST['token']}');\" class='button'>ACK</a></td></tr>";
				$labels[] = $d['label'];
				$alarm[] = "alarmStatus('{$d['label']}', token);\n";
			}
			$alarmsStatus = implode('					  	',$alarm);
			echo "<script>
					const url = 'https://pwma.elettra.eu:10443/v1';
					  function ackAlarm(alarm, token) {
						console.log('ackAlarm(alarm, token)', `\${url}/formula/\${alarm}`, token);
						fetch(`\${url}/formula/\${alarm}`, {
						  method: 'POST',
						  headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							token: token,
						  },
						  body: JSON.stringify({ack: 1}),
						})
						.then(response => {
						   console.log('ackAlarm(alarm, token), response', response);
						   document.location = './pwma_starter.php?alarm_admin&token={$_REQUEST['token']}';
						})
					  }
					  function subscribeAlarm(alarm, token) {
						console.log('subscribeAlarm(alarm, token)', `\${url}/formula/\${alarm}`, token);
						fetch(`\${url}/formula/\${alarm}`, {
						  method: 'SUBSCRIBE',
						  headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							token: token,
							// username: '$username',
						  },
						  body: JSON.stringify({ack: 1}),
						})
						.then(response => {
						  console.log('subscribeAlarm(alarm, token), response', response);
						  document.location = './pwma_starter.php?alarm_admin&token={$_REQUEST['token']}';
						});
					  }
					  function unsubscribeAlarm(alarm, token) {
						console.log('unsubscribeAlarm(alarm, token)', `\${url}/formula/\${alarm}`, token);
						fetch(`\${url}/formula/\${alarm}`, {
						  method: 'UNSUBSCRIBE',
						  headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							token: token,
						  },
						  body: JSON.stringify({ack: 1}),
						})
						.then(response => {
						  console.log('unsubscribeAlarm(alarm, token), response', response);
						  document.location = './pwma_starter.php?alarm_admin&token={$_REQUEST['token']}';
						});
					  }
					  function alarmStatus(alarm, token) {
						console.log(`fetch(\${url}/formula/\${alarm})`, token);
					  	fetch(`\${url}/formula/\${alarm}`, {headers: {token: token}})
						  .then(resp => {
							console.log('response', resp);
							if (resp.status < 300) return resp.json();
							document.getElementById(alarm).style.background = '#fff030';
						  })
						  .then((respJson) => {
							console.log(respJson.acknowledged);
							document.getElementById(alarm).style.background = respJson.acknowledged? 'white': '#e04030';
						  })
						}
					  function alarmsStatus(token) {
					  	$alarmsStatus
					  }
					window.onload = alarmsStatus('{$_REQUEST['token']}');
					</script>\n";
			$data = $sql->sql_secure("SELECT $columns FROM formula WHERE NOT label IN ('".implode("','", $labels)."') AND NOT COALESCE(dateexpiration<NOW(), false) AND username=(SELECT username FROM token WHERE value=$1) ORDER BY label", array($_REQUEST['token']));
			echo "$echoBuf<tr><td colspan='2'>&nbsp;</td></tr><tr><td colspan='2' style=\"font-size: 26; color: #3333ff; font-weight: bold\">My Alarms</td></tr>\n";
			if (!empty($data)) foreach ($data as $d) {
				echo "<tr style='height: 45px;'><td style='align: right;'>{$d['label']}</td><td>&nbsp;<a href='?alarm_admin&subscribe=1&alarm={$d['label']}&token={$_REQUEST['token']}' class='button'>detail</a>&nbsp;
				<a onClick=\"subscribeAlarm('{$d['label']}', '{$_REQUEST['token']}'); /*document.location = './pwma_starter.php?alarm_admin&token={$_REQUEST['token']}';*/\" class='button'>add</a></td></tr>";
			}
			$data = $sql->sql_secure("SELECT $columns FROM formula WHERE NOT label IN ('".implode("','", $labels)."') AND NOT COALESCE(dateexpiration<NOW(), false) AND username<>(SELECT username FROM token WHERE value=$1) ORDER BY username,label", array($_REQUEST['token']));
			echo "<tr><td colspan='2'>&nbsp;</td></tr><tr><td colspan='2' style=\"font-size: 26; color: #3333ff; font-weight: bold\">Other Alarms</td></tr>\n";
			if (!empty($data)) foreach ($data as $d) {
				echo "<tr style='height: 45px;'><td style='align: right;'>{$d['label']}</td><td>&nbsp;<a href='?alarm_admin&subscribe=1&alarm={$d['label']}&token={$_REQUEST['token']}' class='button'>detail</a>&nbsp;
				<a onClick=\"subscribeAlarm('{$d['label']}', '{$_REQUEST['token']}'); document.location = './pwma_starter.php?alarm_admin&token={$_REQUEST['token']}';\" class='button'>add</a></td></tr>";
			}
			die("</table>\n");
		}
		$tokendata = $sql->sql_secure("SELECT * FROM token WHERE username=$1", array($username));
		$alarmCounter = 0;
		$alarmBuffer = '';
		foreach ($tokendata as $i=>$tok) {
			$alarmdata = $sql->sql_secure("SELECT COUNT(*) AS n FROM alarm WHERE tokenid=$1", array($tok['id']));
			if ($alarmdata[0]['n'] == 0) continue;
			$alarmCounter++;
			$token = $tok['value'];
			$alarmBuffer .= "<br><h3>Device $alarmCounter</h3>\ntoken:<pre>{$tok['value']}</pre>\n<a href='?alarm_admin&tokenid={$tok['id']}&token={$tok['value']}'>alarms</a> {$alarmdata[0]['n']}<br><br>\n";
		}
		if ($alarmCounter == 0) {
			echo "<div style='margin: 10'><a href='./formulaeditor/formula_editor.php$alarm' class='button'>Alarm Editor</a><br><br>\n";
			die("Sorry, there isn't any alarm associated to your username");
		}
		if ($alarmCounter == 1) {$_REQUEST['token'] = $token; alarm_admin();}
		echo "<div style='margin: 10'><a href='./formulaeditor/formula_editor.php$alarm' class='button'>Alarm Editor</a><br><br>\n";
		echo "$alarmCounter devices (mobile, tablet etc) are associated to your username.<br>\nPlease select the alarms of one device.<br><br>$alarmBuffer";
		exit(0);
	}

	// ----------------------------------------------------------------
	// check access credentials
	function check_access() {
		global $sql, $username, $yy, $admin;
		$remote = $_SERVER['REMOTE_ADDR'];
		$forwarded = isset($_SERVER['HTTP_X_FORWARDED_FOR'])? $_SERVER['HTTP_X_FORWARDED_FOR']: 0;
		$webtokenExpirationSeconds = 28800; // 8 hh
		if (empty($_REQUEST['username'])) $_REQUEST['username'] = $_REQUEST['du'];
		if (empty($_REQUEST['elettra_ldap_password'])) $_REQUEST['elettra_ldap_password'] = $_REQUEST['delp'];
		if (!empty($_REQUEST['username']) && isset($_SESSION['token']) && isset($_REQUEST['su'])) {
			// die('su');
			$data = $sql->sql_secure("SELECT * FROM userlog_$yy WHERE webtoken=$1 AND EXTRACT(EPOCH FROM NOW()-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", array($_SESSION['token']));
			$username = $data[0]['username'];
			// echo "$username<pre>"; print_r($admin); echo "</pre>{$_REQUEST['username']}<br>\n";
			if (in_array($username, $admin)) {
				$sql->sql_secure("UPDATE userlog_$yy SET username=$1 WHERE webtoken=$2", array($_REQUEST['username'], $_SESSION['token']));
				$data[0]['username'] = $_REQUEST['username'];
				$username = $data[0]['username'];
			}
			if (!empty($data)) {emit_header('PWMA'); return;}
		}
		if (empty($_REQUEST['username']) && isset($_SESSION['token'])) {
			$data = $sql->sql_secure("SELECT * FROM userlog_$yy WHERE webtoken=$1 AND EXTRACT(EPOCH FROM NOW()-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", array($_SESSION['token']));
			$username = $data[0]['username'];
			if (!empty($data)) {emit_header('PWMA'); return;}
		}
		if (!function_exists('ldap_connect')) die("LDAP module not installed in PHP");
		$ds=ldap_connect("abook.elettra.eu");  // must be a valid LDAP server!
		if (!$ds) { 
			die("<h4>Unable to connect to LDAP server</h4>");
		}
		if (isset($_REQUEST['username']) and isset($_REQUEST['elettra_ldap_password'])) {
			$email = $_REQUEST['username'];
			$password = $_REQUEST['elettra_ldap_password'];
			$r=ldap_bind($ds, $email, $password);  
			if ($r!="successful") {header("Location: https://pwma.elettra.eu/"); die("");}
			for ($i=0,$token=""; $i<5; $i++) {
				$token .= sprintf("%02x", rand(0, 256));
			}
			$_SESSION['token'] = $token;
			$data = $sql->sql_secure("INSERT INTO userlog_$yy (date,webtoken,username,ip) VALUES (NOW(),$1,$2,$3)", array($token, $_REQUEST['username'], $remote));
			$username = $_REQUEST['username'];
		}	
		emit_header('PWMA');
	}
	// session_cache_expire(600);
	session_start();
	if (isset($_REQUEST['logout'])) {
		unset($_SESSION['token']);
	}
	if (isset($_REQUEST['alarm_admin']) && !empty($_REQUEST['token'])) {emit_header('PWMA'); alarm_admin();}
	check_access();
	$yy = date('Y');
	if (isset($_REQUEST['gsave'])) {
		// console.log(treeData.replace().replace().replace());
		$replace = array(
			"<img src='./img/up.png'>" => '',
			"<img src='./img/plus.png'>" => '',
			"<img src='./img/down.png'>" => '',
			'"folder":true,"lazy":false,"expanded":true,"children"' => '"data"',
			'"folder":false,"lazy":false,"key"' => '"component"'
		);
		$s = strtr($_REQUEST['treeSaved'], $replace);
		$stree = preg_replace('/\xC2\xA0/', '', $s);
		$data = $sql->sql_secure("UPDATE starter SET screens = $1, datemodified=NOW() WHERE username = $2", array($stree, $username));
		echo $sql->sql_error()."<br>\n";
		$data = $sql->sql_secure("INSERT INTO starterLog_$yy (screens, datemodified, username) VALUES ($1, NOW(), $2)", array($stree, $username));
		echo $sql->sql_error()."<br>\n";
		$_REQUEST['tree_geditor'] = true;
	}
	echo "<div style='padding: 5px;'>\n";
	if (isset($_REQUEST['alarm_admin'])) alarm_admin();
	if (isset($_REQUEST['tree_geditor'])) {
		tree_geditor();
	}
	if (isset($_REQUEST['screen_text_editor'])) {
		screen_text_editor($_REQUEST['screen_text_editor']);
	}
	if (in_array($username, $admin)) {
		echo "<form method='POST' action='./pwma_starter.php'>\nsu- <input type='text' name='username' hint='username'> <input class='btn btn-secondary' type='submit' name='su'></form>\n<a href='./pwma_status.php'>status</a><br>\n";
		if (isset($_REQUEST['tree_editor'])) {
			$treeuser = isset($_REQUEST['treeuser'])? $_REQUEST['treeuser']: $username; 
			if (isset($_REQUEST['save'])) {
				$data = $sql->sql_secure("UPDATE starter SET screens = $1, datemodified=NOW() WHERE username = $2", array($_REQUEST['treedata'], $treeuser));
				echo $sql->sql_error()."<br>\n";
				$data = $sql->sql_secure("INSERT INTO starterLog_$yy (screens, datemodified, username) VALUES ($1, NOW(), $2)", array($_REQUEST['treedata'], $treeuser));
				echo $sql->sql_error()."<br>\n";
			}
			else if (isset($_REQUEST['load'])) {
				$data = $sql->sql_secure("SELECT screens FROM starter WHERE username = $1", array($_REQUEST['treeuser']));
			}
			else {
				$data = $sql->sql_secure("SELECT screens FROM starter WHERE username = $1", array($treeuser));
			}
			echo "<form method='post' action='?tree_editor'><input type='text' name='treeuser' id='treeuser' value='$treeuser'>&nbsp;<input type='submit' name='load' value='load'>&nbsp;<input type='submit' name='save' value='save'><br><br>\n<textarea name='treedata' id='treedata' rows='20' cols='120'>{$data[0]['screens']}</textarea><br><br>\n";
		}
		else {
			echo "<a href='?tree_editor'>Starter administration</a><br><br>\n";
		}
		echo "\n";
	}
	if (in_array($username, $ujive_users)) {
		echo "<a href='https://pwma.elettra.eu/pwma_jive.php' class='button'>&mu;jive</a><br><br>\n";
	}

	echo '
		<a href="?alarm_admin" class="button">Alarms</a><br><br>
				<div id="tree"></div><br>
		<a href="./apk/" class="button">download PWMA Mobile</a><br><br><br> 
		<!-- Placed at the end of the document so the pages load faster -->
		<!-- fancytree -->
		<link href="./lib/fancytree-2.3.0/src/skin-pwma/ui.fancytree.css" class="skinswitcher" rel="stylesheet" type="text/css">
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.dnd.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.table.js" type="text/javascript"></script>
		<script src="./lib/fancytree-2.3.0/src/jquery.fancytree.columnview.js" type="text/javascript"></script>';
		echo "\n<script>
	var treeService = './pwma_starter.php?tree&username=$username';
	var globalVal;
	var globalLabel;

	// INIT
	var \$_GET = getQueryParams(document.location.search);
	initTree(\$_GET);
	console.log('treeService', treeService);

	function popup(location, title, params) {
		newwindow=window.open(location,title,'height=800,width=400');
		if (window.focus) {newwindow.focus()}
		return false;
	}

	function extractTimeseries(dropData) {
		myproperty = dropData.split('</property>');
		mystring = myproperty[0].split('string>');
		myts = mystring[1].split('<');
		return myts[0];
	}

	function initTree(\$_GET) {
		if (!\$('#tree').length) return;
		var source_url = treeService;
		\$(\"#tree\").fancytree({
			autoScroll: true,
			source: {
				url: source_url
			},
			click: function(event, data) {
				if (data.node.key.indexOf('_')!==0) {
					console.log('click::data', data.node.key);
					if (data.node.key.indexOf('http')===0) {
						window.location = data.node.key;
					}
					else {
						window.location = 'http://pwma.elettra.eu/cordova/index.html?version='+data.node.key;
					}
				}
				return true;// Allow default processing
			},
			clickFolderMode: 2,
			persist: true
		});
	}

	function getQueryParams(qs) {
		qs = qs.split(\"+\").join(\" \");
		var params = {},
				tokens,
				re = /[?&]?([^=]+)=([^&]*)/g;
		while (tokens = re.exec(qs)) {
			params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
		}
		return params;
	}
</script>";
	echo "<h2>Editors</h2><table>\n";
	if (in_array($username, $admin)) {
		echo "<tr><td><h3>Screen</h3></td><td><a href='http://pwma.elettra.eu/designer.html?username=$username' class='button'>Graphic</a></td><td><a href='https://pwma.elettra.eu/pwma_starter.php?screen_text_editor' class='button'>Text</a></td></tr>\n\n";
	}
	else {
		// echo "<tr><td><h3>Screen</h3><td>&nbsp;</td><td><a href='https://pwma.elettra.eu/pwma_starter.php?screen_text_editor' class='button'>Text</a></td></tr>\n\n";
	}
	echo "<tr><td><h3>Starter</h3><td><a href='?tree_geditor' class='button'>Graphic</a></td><td><a href='?tree_geditor&text' class='button'>Text</a></td></tr>\n</table><br><br>\n";
	exit(0);

?>
