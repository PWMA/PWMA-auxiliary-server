DROP TABLE IF EXISTS alarm;
DROP TABLE IF EXISTS alarmLog;
DROP TABLE IF EXISTS formulaLog;
DROP TABLE IF EXISTS formula;
DROP TABLE IF EXISTS token;
DROP TABLE IF EXISTS domain;
DROP TABLE IF EXISTS commandLog;
DROP TABLE IF EXISTS userLog;
DROP TABLE IF EXISTS userScreen;

CREATE TABLE userScreen(
  username TEXT,
  screens TEXT,
  dateModified timestamp,
  PRIMARY KEY (username)
);

CREATE TABLE userLog(
  username TEXT,
  ip inet,
  webtoken varchar(100),
  date timestamp,
  PRIMARY KEY (username, date)
);

CREATE TABLE commandLog(
  command TEXT,
  parameters json,
  tokenId INT,
  username TEXT,
  ip inet,
  date timestamp,
  PRIMARY KEY (command, username, date)
);

CREATE TABLE domain(
  label varchar(20),
  value TEXT,
  PRIMARY KEY (label)
);
INSERT INTO domain VALUES 
('Elettra', 'tango://tom.ecs.elettra.trieste.it:20000'),
('FERMI', 'tango://srv-tango-srf.fcs.elettra.trieste.it:20000'),
('PADReS', 'tango://srv-padres-srf.fcs.elettra.trieste.it:20000'),
('TIMER', 'tango://srv-tmr-srf.fcs.elettra.trieste.it:20000'),
('TIMEX', 'tango://srv-tmx-srf.fcs.elettra.trieste.it:20000'),
('LDM', 'tango://srv-ldm-srf.fcs.elettra.trieste.it:20000'),
('DiProI', 'tango://srv-diproi-srf.fcs.elettra.trieste.it:20000'),
('Magnedyn', 'tango://srv-mb-srf.fcs.elettra.trieste.it:20000'),
('TeraFERMI', 'tango://srv-tf-srf.fcs.elettra.trieste.it:20000');

CREATE TABLE token(
  id SERIAL,
  value TEXT,
  username TEXT,
  PRIMARY KEY (id)
);
CREATE TABLE formula(
  label varchar(20) NOT NULL,
  formula text NOT NULL,
  note text,
  datecreated timestamp NOT NULL,
  datemodified timestamp,
  ip inet,
  username text NOT NULL,
  dateexpiration timestamp,
  domain varchar(20),
  system varchar(100),
  delay float4,
  FOREIGN KEY (domain) REFERENCES domain(label),
  PRIMARY KEY (label)
);
CREATE TABLE formulaLog(
  label varchar(20),
  formula TEXT,
  tokenId INT,
  username TEXT,
  ip inet,
  date timestamp,
  dateExpiration timestamp,
  PRIMARY KEY (label, date)
);
CREATE TABLE alarmLog(
  label varchar(20),
  tokenId INT,
  date timestamp,
  ack boolean,
  subscribe boolean,
  FOREIGN KEY (label) REFERENCES formula(label),
  FOREIGN KEY (tokenId) REFERENCES token(id),
  PRIMARY KEY (label, tokenId, date)
);
CREATE TABLE alarm(
  label varchar(20),
  tokenId INT,
  FOREIGN KEY (label) REFERENCES formula(label),
  FOREIGN KEY (tokenId) REFERENCES token(id),
  PRIMARY KEY (label, tokenId)
);
CREATE TABLE cmdlog_2019 (
  username text NOT NULL,
  date timestamp,
  ip inet NOT NULL,
  cmd text,
  param text,
  PRIMARY KEY (username, date, cmd)
);
DROP TABLE userscreenlog_2019;
CREATE TABLE userscreenlog_2019 (
  username varchar(50),
  screenid INT,
  filename varchar(100),
  date timestamp,
  ip inet,
  PRIMARY KEY (username, screenid, filename, date)
);
CREATE TABLE cmdunrestricted (
  src varchar(250),
  PRIMARY KEY (src)
);

