<?php
	function exec_cmd($var, $value) {
		$url = "https://pwma.elettra.eu:10443/v1/cs/$var";
		$fields = array('value' => $value);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		if (!empty($value)) curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$err = '';
		$result = curl_exec($ch);
		if ($result === false) {
			$err = curl_error($ch); // .' - '.curl_strerror($errno);
		}
		curl_close($ch);
		return array($result, $err);
	}
	require_once("./conf.php");
	$sql = open_db();	
	$cmdunrestricted = $sql->sql_secure("SELECT * FROM cmdunrestricted WHERE src=$1", array($_REQUEST['cmd']));
	if (empty($cmdunrestricted)) {
		session_start();
		$username = '';
		if (isset($_SESSION['token'])) {
			$yy = date('Y');
			$webtokenExpirationSeconds = 36000; // 10 hh
			$data = $sql->sql_secure("SELECT * FROM userlog_$yy WHERE webtoken=$1 AND EXTRACT(EPOCH FROM NOW()-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", array($_SESSION['token']));
			if (!empty($data)) $username = $data[0]['username'];
		}

		if (empty($username)) {
			if (!function_exists('ldap_connect')) die(json_encode(array('error'=>"LDAP module not installed in PHP")));
			$ds = ldap_connect("abook.elettra.eu");  // must be a valid LDAP server!
			if (!$ds) { 
				die(json_encode(array('error'=>"Unable to connect to LDAP server")));
			}
			if (empty($_SERVER['PHP_AUTH_USER'])) {
				die(json_encode(array('error'=>"Empty username")));
			}
			else {
				$username = $_SERVER['PHP_AUTH_USER'];
				$password = $_SERVER['PHP_AUTH_PW'];
				$r = ldap_bind($ds, $username, $password);  
				if ($r!="successful") die(json_encode(array('error'=>"authentication failed")));
			}
		}
	}	
	$value = is_numeric($_REQUEST['params'])? $_REQUEST['params']-0: $_REQUEST['params'];
	$time = date('d/m/Y H:i:s');
	list($result, $err) = exec_cmd($_REQUEST['cmd'], $value);
	if ($result === false) {echo json_encode(array('result'=>"ERROR, curl_exec() returned: $err", 'time'=>$time)); }
	else echo json_encode(array('result'=>$result, 'time'=>$time));
	$yy = date('Y');
	if (empty($cmdunrestricted)) {
		$sql->sql_secure("INSERT INTO cmdlog_$yy (username, date, ip, cmd, param) VALUES ($1, NOW(), $2, $3, $4)", array($username, $_SERVER['REMOTE_ADDR'], $_REQUEST['cmd'], empty($_REQUEST['params'])? '': $_REQUEST['params']));
		$query = "INSERT INTO cmdlog_$yy (username, date, ip, cmd, param) VALUES ('$username', '$time', '{$_SERVER['REMOTE_ADDR']}', '{$_REQUEST['cmd']}', '{$_REQUEST['params']}');";
		$myfile = file_put_contents('cmd_log.sql', $query.PHP_EOL , FILE_APPEND | LOCK_EX);
	}
?>