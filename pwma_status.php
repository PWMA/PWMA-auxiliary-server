<?php
	require_once("./conf.php");
	$sql = open_db();
	ini_set('session.gc_maxlifetime', 8*3600);
	session_set_cookie_params(8*3600);
	session_start();
	$username = '';
	if (isset($_SESSION['token'])) {
		$webtokenExpirationSeconds = 28800; // 8 hh
		$stmt = $sql->sql_prepare("SELECT * FROM userlog WHERE webtoken=$1 AND EXTRACT(EPOCH FROM NOW()-date)<$webtokenExpirationSeconds ORDER BY date DESC LIMIT 1", "q1");
		$data = $sql->sql_execute(array($_SESSION['token']), "q1");
		$username = $data[0]['username'];
	}
	if ($username !== 'admin') die(""); 
	if (isset($_REQUEST['users'])) { 
		$data = $sql->sql_select('*', 'token', ($_REQUEST['users']=='all')? '1=1 ORDER BY username': 'id IN (SELECT DISTINCT tokenid FROM alarm) ORDER BY username');
		echo ($_REQUEST['users']=='all')? 'All ': 'Alarm subscribed ';
		echo "Users:<br><table>\n<tr><td>username</td><td>id</td><td>token</td></tr>";
		foreach ($data as $d) {
			echo "<tr><td>{$d['username']}</td><td><a href='?tokenid={$d['id']}&username={$d['username']}'>{$d['id']}</a></td><td>{$d['value']}</td></tr>";
		}
		die("</table><br>View ".(($_REQUEST['users']=='all')? "<a href='?users'>Alarm subscribed users</a>\n": "<a href='?users=all'>all users</a>\n"));
    }
	if (isset($_REQUEST['tokenid'])) {
		$username = strip_tags($_REQUEST['username']);
		echo "$username, tokenid: {$_REQUEST['tokenid']}<br>\n";
		if (isset($_REQUEST['alarm'])) {
			$stmt = $sql->sql_prepare("SELECT value FROM token WHERE id=$1", "q6");
			$data = $sql->sql_execute(array($_REQUEST['tokenid']), "q6");
			$token = $data[0]['value'];
			echo strip_tags($_REQUEST['alarm']);
			$stmt = $sql->sql_prepare("SELECT * FROM formula WHERE label=$1", "q5");
			$data = $sql->sql_execute(array($_REQUEST['alarm']), "q5");
			echo "<pre>"; print_r($data[0]); echo "</pre>
				<script>
				  function ackAlarm(alarm, token) {
				  	const url = 'https://pwma.elettra.eu:10443/v1';
					console.log('ackAlarm(alarm, token)', `\${url}/formula/\${alarm} {${token}}`);
					fetch(`\${url}/formula/\${alarm}`, {
					  method: 'POST',
					  headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						token: token,
					  },
					  body: JSON.stringify({ack: 1}),
					})
					.then(response => {
					  console.log('ackAlarm(alarm, token), response', response);
				  	})
				  }
					  function subscribeAlarm(alarm, token) {
						const url = 'https://pwma.elettra.eu:10443/v1';
						console.log('subscribeAlarm(alarm, token)', `\${url}/formula/\${alarm}`, token);
						fetch(`\${url}/formula/\${alarm}`, {
						  method: 'SUBSCRIBE',
						  headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							token: token,
							// username: '$username',
						  },
						  body: JSON.stringify({ack: 1}),
						})
						.then(response => {
						  console.log('subscribeAlarm(alarm, token), response', response);
						});
					  }
					  function unsubscribeAlarm(alarm, token) {
						const url = 'https://pwma.elettra.eu:10443/v1';
						console.log('unsubscribeAlarm(alarm, token)', `\${url}/formula/\${alarm}`, token);
						fetch(`\${url}/formula/\${alarm}`, {
						  method: 'UNSUBSCRIBE',
						  headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							token: token,
						  },
						  body: JSON.stringify({ack: 1}),
						})
						.then(response => {
						  console.log('unsubscribeAlarm(alarm, token), response', response);
						});
					  }
				</script>
				<button onClick=\"ackAlarm('{$_REQUEST['alarm']}', '$token')\">ACK</button>
				<button onClick=\"subscribeAlarm('{$_REQUEST['alarm']}', '$token')\">Subscribe</button>
				<button onClick=\"unsubscribeAlarm('{$_REQUEST['alarm']}', '$token')\">Unsubscribe</button>
			<br>\n";
			$stmt = $sql->sql_prepare("SELECT * FROM alarmlog WHERE tokenId=$1 AND label=$2 ORDER BY date DESC", "q3");
			$data = $sql->sql_execute(array($_REQUEST['tokenid'],$_REQUEST['alarm']), "q3");
			echo "<table>\n<tr><td>date</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>ack&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>subscribe</td></tr>";
			if (!empty($data)) foreach ($data as $d) {
				echo "<tr><td> {$d['date']} </td><td>&nbsp;</td><td> {$d['ack']} </td><td> {$d['subscribe']} </td></tr>\n";
			}
			die("</table>\n");
		}
		if (isset($_REQUEST['delete'])) {
			$stmt = $sql->sql_prepare("DELETE FROM alarm WHERE tokenId=$1", "q2");
			$data = $sql->sql_execute(array($_REQUEST['tokenid']), "q2");
			echo "Deleted all alarms of $username<br><br>\n";
		}
		$stmt = $sql->sql_prepare("SELECT * FROM alarm WHERE tokenId=$1 ORDER BY label", "q4");
		$data = $sql->sql_execute(array($_REQUEST['tokenid']), "q4");
		echo "<a href='?tokenid={$_REQUEST['tokenid']}&delete'>delete</a><table>\n<tr><td>registered alarms:</td></tr>\n";
		if (!empty($data)) foreach ($data as $d) {
			echo "<tr><td><a href='?alarm={$d['label']}&tokenid={$d['tokenid']}&username=$username'>{$d['label']}</a></td></tr>";
		}
		die("</table>\n");
    }
	if (isset($_REQUEST['curl'])) {
		$ctx = stream_context_create(array('http'=>array('timeout' => 1)));
		$domains = array("srv-tango-srf.fcs.elettra.trieste.it","tom.ecs.elettra.trieste.it","srv-padres-srf.fcs.elettra.trieste.it","srv-ldm-srf.fcs.elettra.trieste.it","srv-tmx-srf.fcs.elettra.trieste.it","srv-tf-srf.fcs.elettra.trieste.it","srv-diproi-srf.fcs.elettra.trieste.it","srv-tmr-srf.fcs.elettra.trieste.it");
		$stat = array();
		foreach ($domains as $domain) {
			$stat[$domain] = array('curl'=>strlen(file_get_contents("https://pwma.elettra.eu:10443/v1/cs/tango://$domain:20000/*", false, $ctx)));
			exec("ping -c 1 $domain | grep received", $pingres, $pingerr);
			// echo "$domain<pre>"; print_r($pingres); echo "</pre><br>\n";
			$pingArray = explode(',',$pingres[0]);
			$stat[$domain]['ping'] = $pingerr==0 && strpos($pingArray[1], '1 received')!==false;
			unset($pingres);
			unset($pingerr);
		}
		echo "<pre>"; print_r($stat); echo "</pre><br>\n";
		exit(0);
	}
	if (isset($_REQUEST['sleep'])) {
		if ($_REQUEST['sleep']>0) sleep($_REQUEST['sleep']);
		die("sleep ".$_REQUEST['sleep']);
	}
	if (isset($_REQUEST['pidof'])) {
		exec('pidof canoned', $res, $err);
		$alarm = $err>0 || !(isset($res[0]));
		/*
		exec('ping -c 1 log | grep received', $pingres, $pingerr);
		$pingArray = explode(',',$pingres[0]);
		$ping = $pingerr==0 && strpos($pingArray[1], '1 received')!==false;
		*/
		// echo "ping: ".($ping?"OK":"NOK")."<br>err: $err<pre>"; print_r($pingres); print_r($pingArray); echo "</pre><br>\n";
		die("
		<html><head>
			  <title>PWMA status</title>
			  <meta http-equiv='refresh' content='10;?pidof'>".($alarm?"<link rel='SHORTCUT ICON' href='./img/led_yellow.gif' />": '')."
			  <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
			  </head>
			  <body>".($alarm? "ERRORE, server canoned non funzionante": "OK")."
		</body></html>");
	}
	$s = file_get_contents("http://pwma.elettra.eu:10080/status");
	$status = json_decode($s);
	echo "<a href='?users'>users</a><br>\n<!--a href='./log/'>logs</a><br-->\n<pre>"; print_r($status); echo "</pre><br>\n";
?>
