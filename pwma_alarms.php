<?php
	header("Content-Type: application/json");
	require_once("./conf.php");
	$sql = open_db();

	function multiexplode ($delimiters,$string) {
		$ready = str_replace($delimiters, $delimiters[0], $string);
		$launch = explode($delimiters[0], $ready);
		return  $launch;
	}

	if (isset($_REQUEST['list'])) {
		$s = array();
		$columns = 'label,formula,note,username,domain,system,dateexpiration,delay';
		if (isset($_REQUEST['token'])) {
			$stmt = $sql->sql_prepare("SELECT label FROM alarm, token WHERE tokenId=token.id AND token.value=$1 ORDER BY label", "q1");
			$data = $sql->sql_execute(array($_REQUEST['token']), "q1");
			if (isset($_REQUEST['nojson'])) {echo "\n<pre>"; print_r($data); echo "</pre>";}
			foreach ($data as $d) {$s[] = $d['label'];}
			if (isset($_REQUEST['nojson'])) {echo "\n<pre>"; print_r($s); echo "</pre>";}
			$labels = "'".implode("','", $s)."'";
			$data = $sql->sql_select($columns, 'formula', "label IN ($labels)");
			foreach ($data as $d) {$subscribed[$d['label']] = $d;}
			$mine = $others = array();
			if (isset($_REQUEST['username'])) {
				$stmt = $sql->sql_prepare("SELECT $columns FROM formula WHERE NOT label IN ($labels) AND NOT COALESCE(dateexpiration<NOW(), false) AND username=$1 ORDER BY label", "q2");
				$data = $sql->sql_execute(array($_REQUEST['username']), "q2");
				foreach ($data as $d) {$mine[$d['label']] = $d;}
				$stmt = $sql->sql_prepare("SELECT $columns FROM formula WHERE NOT label IN ($labels) AND NOT COALESCE(dateexpiration<NOW(), false) AND username<>$1 ORDER BY username,label", "q3");
				$data = $sql->sql_execute(array($_REQUEST['username']), "q3");
				foreach ($data as $d) {$others[$d['label']] = $d;}
			}
			else {
				$data = $sql->sql_select($columns, 'formula', "NOT label IN ($labels) AND NOT COALESCE(dateexpiration<NOW(), false)");
				foreach ($data as $d) {$others[$d['label']] = $d;}
			}
			die(json_encode(array('subscribed'=>$subscribed, 'mine'=>$mine, 'others'=>$others)));
		}
		// $data = $sql->sql_select('label,formula,username', 'formula', '1=1');
		$data = $sql->sql_select('*', 'formula', '1=1');
		if (isset($_REQUEST['nojson'])) {echo "\n<pre>"; print_r($data); echo "</pre>";}
		if (isset($_REQUEST['debug'])) {die(json_encode(explode("\n",$sql->sql_error())));}
		foreach ($data as $d) {
			$s[] = $d; // array('label'=>$d['label'], 'formula'=>$d['formula']);
		}
		die(json_encode($s));
	}
	if (isset($_REQUEST['open']) and (strlen($_REQUEST['open']) < 100)) {
		$stmt = $sql->sql_prepare("SELECT * FROM formula WHERE name=$1", "q1");
		$params = array($_REQUEST['open']);
		$data = $sql->sql_execute($params, "q1");
		/*
		$formulaArray = explode($data[0]['formula'],'tango://');
		foreach ($formulaArray as $k=>$f) {
			$t = explode('/', $f);
			$e = multiexplode(array(' ','<','>','+','-','+','*','|','&'),$t[4]);
			$t[4] = strtr($t[4], array($e[0]=>e[0].'"'));
			$formulaArray[$k] = implode('/', $t);
		}
		die(json_encode(implode('"tango://',$formulaArray)));
		*/
		die(json_encode($data[0]['formula']));
	}
	if (isset($_REQUEST['save'])) {
		// NOTIFY channel='alarm' [ , payload ] https://www.postgresql.org/docs/9.0/static/sql-notify.html
		session_start();
		$note = isset($_REQUEST['note'])? strip_tags($_REQUEST['note']): null;
		$dateexpiration = !empty($_REQUEST['dateexpiration'])? strip_tags($_REQUEST['dateexpiration']): null;
		$stmt3 = $sql->sql_prepare("SELECT username, ip FROM userlog WHERE webtoken=$1", "q3");
		$data = $sql->sql_execute(array($_SESSION['token']), "q3");
		if (empty($data)) die(json_encode("User validation failed").$sql->sql_error());
		$username = $data[0]['username'];
		if (strpos($_REQUEST['name'], '.')!==false) die('Invalid alarm name.');
		$stmt4 = $sql->sql_prepare("SELECT username FROM formula WHERE label=$1", "q4");
		$data = $sql->sql_execute(array($_REQUEST['name']), "q4");
		if (isset($_REQUEST['debug'])) {echo "data1: "; print_r($data);echo "- $stmt4 - ".$sql->sql_error();}
		$insert = empty($data) || $data[0]['username']!==$username;
		$stmt5 = $sql->sql_prepare("SELECT username,label FROM formula WHERE formula=$1", "q5");
		$data = $sql->sql_execute(array(strtr($_REQUEST['formula'], array('"'=>''))), "q5"); // strip_tags(
		if (isset($_REQUEST['debug'])) {echo "data2: "; print_r($data);echo "- $stmt5 - ".$sql->sql_error();}
		$delay = !empty($_REQUEST['delay'])? $_REQUEST['delay']: null;
		if ($insert && (!empty($data) && ($data[0]['username']===$username))) {
			$query = "UPDATE formula SET formula=formula || ' ',dateexpiration=NOW() WHERE label='".$data[0]['label']."'";
			$stmt = $sql->sql_query($query);
		}
		if ($insert) {
			$query = "INSERT INTO formula (username,ip,formula,note,dateexpiration,delay,label) VALUES ($1, $2, $3, $4, $5, $6, $7)";
		}
		else {
			$query = "UPDATE formula SET username=$1,ip=$2,formula=$3,note=$4,dateexpiration=$5,delay=$6 WHERE label=$7";
		}
		$stmt = $sql->sql_prepare($query, "q2");
		$err = $sql->sql_error();
		$params = array(strip_tags($username), $_SERVER['REMOTE_ADDR'], strtr($_REQUEST['formula'], array('>'=>' > ','<'=>' < ','"'=>'','&lt'=>'','&gt'=>'','&amp'=>'','  '=>' ')), $note, $dateexpiration, $delay, strip_tags($_REQUEST['name']));
		$data = $sql->sql_execute($params, "q2");
		if (isset($_REQUEST['debug'])) echo $sql->sql_error()."\n$query\n".json_encode($params);
		echo json_encode("$err saving alarm {$_REQUEST['name']} ".$sql->sql_error());
	}
	if (isset($_REQUEST['history'])) {
		if (isset($_REQUEST['label']) && isset($_REQUEST['token'])) {
			$stmt = $sql->sql_prepare('SELECT id FROM token WHERE value=$1', "q1");
			if (isset($_REQUEST['debug'])) echo json_encode($sql->sql_error());
			$data = $sql->sql_execute(array($_REQUEST['token']), "q1");
			if (!empty($data)) {
				$tokenid = $data[0]['id'];
				$stmt2 = $sql->sql_prepare("SELECT date, ack, subscribe FROM alarmlog WHERE label=$1 AND tokenid=$tokenid ORDER BY date DESC LIMIT 20", "q2");
				$data = $sql->sql_execute(array($_REQUEST['label']), "q2");
				die(json_encode($data));
			}
		}
		die(json_encode("Empty history"));
	}
	if (isset($_REQUEST['delete'])) {
		$stmt = $sql->sql_prepare("DELETE FROM formula WHERE label=$1", "q1");
		$params = array($_REQUEST['delete']);
		$data = $sql->sql_execute($params, "q1");
		echo json_encode($sql->sql_error());
	}
	if (isset($_REQUEST['update'])) {
		if (isset($_REQUEST['formula'])) {
			$stmt = $sql->sql_prepare("UPDATE formula SET formula=$1, datemodified=NOW() WHERE label=$2 AND username=$3", "q1");
			$params = array(strip_tags($_REQUEST['formula']),$_REQUEST['label'],$_REQUEST['username']);
			$notify = "NOTIFY \"alarm\", { \"event\": \"update\", \"data\": {\"label\": \"{$_REQUEST['label']}\",\"token\": \"{$_REQUEST['token']}\",  \"formula\": \"{$_REQUEST['formula']}\"}}";
		}
		else if (isset($_REQUEST['dateexpiration'])) {
			$stmt = $sql->sql_prepare("UPDATE formula SET dateexpiration=$1, datemodified=NOW() WHERE label=$2 AND username=$3", "q1");
			$params = array(strip_tags($_REQUEST['dateexpiration']),$_REQUEST['label'],$_REQUEST['username']);
			$notify = "NOTIFY \"alarm\", { \"event\": \"update\", \"data\": {\"label\": \"{$_REQUEST['label']}\",\"token\": \"{$_REQUEST['token']}\",  \"dateexpiration\": \"{$_REQUEST['dateexpiration']}\"}}";
		}
		else if (isset($_REQUEST['delay'])) {
			$stmt = $sql->sql_prepare("UPDATE formula SET delay=$1, datemodified=NOW() WHERE label=$2 AND username=$3", "q1");
			$params = array(strip_tags($_REQUEST['delay']),$_REQUEST['label'],$_REQUEST['username']);
			$notify = "NOTIFY \"alarm\", { \"event\": \"update\", \"data\": {\"label\": \"{$_REQUEST['label']}\",\"token\": \"{$_REQUEST['token']}\",  \"delay\": \"{$_REQUEST['delay']}\"}}";
		}
		else if (isset($_REQUEST['note'])) {
			$stmt = $sql->sql_prepare("UPDATE formula SET note=$1, datemodified=NOW() WHERE label=$2 AND username=$3", "q1");
			$params = array(strip_tags($_REQUEST['note']),$_REQUEST['label'],$_REQUEST['username']);
			$notify = "NOTIFY \"alarm\", { \"event\": \"update\", \"data\": {\"label\": \"{$_REQUEST['label']}\",\"token\": \"{$_REQUEST['token']}\",  \"note\": \"{$_REQUEST['note']}\"}}";
		}
		$data = $sql->sql_execute($params, "q1");
		if (!$sql->sql_error()) {
			// $sql->sql_query($notify);
			$stmt = $sql->sql_prepare('SELECT id FROM token WHERE value=$1', "q2");
			if (isset($_REQUEST['debug'])) echo json_encode($sql->sql_error());
			$data = $sql->sql_execute(array($_REQUEST['token']), "q2");
			if (isset($_REQUEST['debug'])) echo json_encode($data);

			$stmt = $sql->sql_prepare("INSERT INTO formulalog (username,ip,label,formula,tokenid,date,dateexpiration,delay) VALUES ($1, $2, $3, $4, {$data[0]['id']}, NOW(), $5, $6)", "q3");
			if (isset($_REQUEST['debug'])) echo json_encode($sql->sql_error());
			$username = isset($_REQUEST['username'])? $_REQUEST['username']: null;
			$formula = isset($_REQUEST['formula'])? $_REQUEST['formula']: null;
			$delay = isset($_REQUEST['delay'])? $_REQUEST['delay']: null;
			$dateexpiration = isset($_REQUEST['dateexpiration'])? $_REQUEST['dateexpiration']: null;
			$params = array($username, $_SERVER['REMOTE_ADDR'], $_REQUEST['label'], $formula, $dateexpiration, $delay);
			$data = $sql->sql_execute($params, "q3");
		}
		echo json_encode($sql->sql_error());
	}
?>
